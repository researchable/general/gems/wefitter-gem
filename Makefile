.DEFAULT_GOAL := all

all: clean create build

create:
	swagger-codegen generate -i https://api.wefitter.com/api/v1.3/redoc/\?format\=openapi -l ruby -c config.json -o wefitter-gem

run:
	docker compose up

build:
	echo 'perform building'
	cd wefitter-gem
	# For some reason we keep 'endend' in some files after generation, so we need to fix it.
	find . -type f -name '*.rb' -exec sed -i 's/endend/end\nend/g' {} +

clean:
	echo 'perform cleaning'
	rm -rf wefitter-gem
