=begin
#WeFitter API

#This is the WeFitter API

OpenAPI spec version: v1.3
Contact: hello@wefitter.com
Generated by: https://github.com/swagger-api/swagger-codegen.git
Swagger Codegen version: 2.4.38
=end

require 'date'

module WeFitter
  class ChallengeDetail
    attr_accessor :url

    attr_accessor :public_id

    attr_accessor :title

    attr_accessor :slogan

    attr_accessor :description

    attr_accessor :start

    attr_accessor :_end

    attr_accessor :type

    attr_accessor :goal

    attr_accessor :goal_value

    attr_accessor :goal_type

    attr_accessor :visibility

    attr_accessor :num_members

    attr_accessor :calculation_method

    # Progress towards the goal, from perspective of profile whose bearer is used for authentication: can be an individual/team/global score. When goal is duration, this is the duration in seconds. When an admin token is used, returns 0.0.
    attr_accessor :goal_progress

    attr_accessor :enrollment_method

    attr_accessor :default_enrollment_time

    attr_accessor :repetition

    attr_accessor :avatar

    attr_accessor :data_source

    attr_accessor :activity_types

    # The start (exclusive) of the current period. Null if the challenge has not started yet.
    attr_accessor :current_period_start

    # The end (exclusive) of the current period. Null if the challenge end has passed.
    attr_accessor :current_period_end

    # Whether scores should be unable to surpass the goal value.
    attr_accessor :goal_cap

    attr_accessor :total_calories

    attr_accessor :total_distance

    attr_accessor :total_steps

    attr_accessor :total_points

    class EnumAttributeValidator
      attr_reader :datatype
      attr_reader :allowable_values

      def initialize(datatype, allowable_values)
        @allowable_values = allowable_values.map do |value|
          case datatype.to_s
          when /Integer/i
            value.to_i
          when /Float/i
            value.to_f
          else
            value
          end
        end
      end

      def valid?(value)
        !value || allowable_values.include?(value)
      end
    end

    # Attribute mapping from ruby-style variable name to JSON key.
    def self.attribute_map
      {
        :'url' => :'url',
        :'public_id' => :'public_id',
        :'title' => :'title',
        :'slogan' => :'slogan',
        :'description' => :'description',
        :'start' => :'start',
        :'_end' => :'end',
        :'type' => :'type',
        :'goal' => :'goal',
        :'goal_value' => :'goal_value',
        :'goal_type' => :'goal_type',
        :'visibility' => :'visibility',
        :'num_members' => :'num_members',
        :'calculation_method' => :'calculation_method',
        :'goal_progress' => :'goal_progress',
        :'enrollment_method' => :'enrollment_method',
        :'default_enrollment_time' => :'default_enrollment_time',
        :'repetition' => :'repetition',
        :'avatar' => :'avatar',
        :'data_source' => :'data_source',
        :'activity_types' => :'activity_types',
        :'current_period_start' => :'current_period_start',
        :'current_period_end' => :'current_period_end',
        :'goal_cap' => :'goal_cap',
        :'total_calories' => :'total_calories',
        :'total_distance' => :'total_distance',
        :'total_steps' => :'total_steps',
        :'total_points' => :'total_points'
      }
    end

    # Attribute type mapping.
    def self.swagger_types
      {
        :'url' => :'String',
        :'public_id' => :'String',
        :'title' => :'String',
        :'slogan' => :'String',
        :'description' => :'String',
        :'start' => :'DateTime',
        :'_end' => :'DateTime',
        :'type' => :'String',
        :'goal' => :'String',
        :'goal_value' => :'Integer',
        :'goal_type' => :'String',
        :'visibility' => :'String',
        :'num_members' => :'String',
        :'calculation_method' => :'String',
        :'goal_progress' => :'Float',
        :'enrollment_method' => :'String',
        :'default_enrollment_time' => :'String',
        :'repetition' => :'String',
        :'avatar' => :'String',
        :'data_source' => :'String',
        :'activity_types' => :'Array<ChallengeActivityType>',
        :'current_period_start' => :'DateTime',
        :'current_period_end' => :'DateTime',
        :'goal_cap' => :'BOOLEAN',
        :'total_calories' => :'Float',
        :'total_distance' => :'Float',
        :'total_steps' => :'Float',
        :'total_points' => :'Float'
      }
    end

    # Initializes the object
    # @param [Hash] attributes Model attributes in the form of hash
    def initialize(attributes = {})
      return unless attributes.is_a?(Hash)

      # convert string to symbol for hash key
      attributes = attributes.each_with_object({}) { |(k, v), h| h[k.to_sym] = v }

      if attributes.has_key?(:'url')
        self.url = attributes[:'url']
      end

      if attributes.has_key?(:'public_id')
        self.public_id = attributes[:'public_id']
      end

      if attributes.has_key?(:'title')
        self.title = attributes[:'title']
      end

      if attributes.has_key?(:'slogan')
        self.slogan = attributes[:'slogan']
      end

      if attributes.has_key?(:'description')
        self.description = attributes[:'description']
      end

      if attributes.has_key?(:'start')
        self.start = attributes[:'start']
      end

      if attributes.has_key?(:'end')
        self._end = attributes[:'end']
      end

      if attributes.has_key?(:'type')
        self.type = attributes[:'type']
      end

      if attributes.has_key?(:'goal')
        self.goal = attributes[:'goal']
      end

      if attributes.has_key?(:'goal_value')
        self.goal_value = attributes[:'goal_value']
      end

      if attributes.has_key?(:'goal_type')
        self.goal_type = attributes[:'goal_type']
      end

      if attributes.has_key?(:'visibility')
        self.visibility = attributes[:'visibility']
      end

      if attributes.has_key?(:'num_members')
        self.num_members = attributes[:'num_members']
      end

      if attributes.has_key?(:'calculation_method')
        self.calculation_method = attributes[:'calculation_method']
      end

      if attributes.has_key?(:'goal_progress')
        self.goal_progress = attributes[:'goal_progress']
      end

      if attributes.has_key?(:'enrollment_method')
        self.enrollment_method = attributes[:'enrollment_method']
      end

      if attributes.has_key?(:'default_enrollment_time')
        self.default_enrollment_time = attributes[:'default_enrollment_time']
      end

      if attributes.has_key?(:'repetition')
        self.repetition = attributes[:'repetition']
      end

      if attributes.has_key?(:'avatar')
        self.avatar = attributes[:'avatar']
      end

      if attributes.has_key?(:'data_source')
        self.data_source = attributes[:'data_source']
      end

      if attributes.has_key?(:'activity_types')
        if (value = attributes[:'activity_types']).is_a?(Array)
          self.activity_types = value
        end
      end

      if attributes.has_key?(:'current_period_start')
        self.current_period_start = attributes[:'current_period_start']
      end

      if attributes.has_key?(:'current_period_end')
        self.current_period_end = attributes[:'current_period_end']
      end

      if attributes.has_key?(:'goal_cap')
        self.goal_cap = attributes[:'goal_cap']
      end

      if attributes.has_key?(:'total_calories')
        self.total_calories = attributes[:'total_calories']
      end

      if attributes.has_key?(:'total_distance')
        self.total_distance = attributes[:'total_distance']
      end

      if attributes.has_key?(:'total_steps')
        self.total_steps = attributes[:'total_steps']
      end

      if attributes.has_key?(:'total_points')
        self.total_points = attributes[:'total_points']
      end
    end

    # Show invalid properties with the reasons. Usually used together with valid?
    # @return Array for valid properties with the reasons
    def list_invalid_properties
      invalid_properties = Array.new
      if @title.nil?
        invalid_properties.push('invalid value for "title", title cannot be nil.')
      end

      if @title.to_s.length < 1
        invalid_properties.push('invalid value for "title", the character length must be great than or equal to 1.')
      end

      if @start.nil?
        invalid_properties.push('invalid value for "start", start cannot be nil.')
      end

      if @type.nil?
        invalid_properties.push('invalid value for "type", type cannot be nil.')
      end

      if @goal.nil?
        invalid_properties.push('invalid value for "goal", goal cannot be nil.')
      end

      if !@goal_value.nil? && @goal_value > 2147483647
        invalid_properties.push('invalid value for "goal_value", must be smaller than or equal to 2147483647.')
      end

      if !@goal_value.nil? && @goal_value < -2147483648
        invalid_properties.push('invalid value for "goal_value", must be greater than or equal to -2147483648.')
      end

      if @goal_type.nil?
        invalid_properties.push('invalid value for "goal_type", goal_type cannot be nil.')
      end

      if @visibility.nil?
        invalid_properties.push('invalid value for "visibility", visibility cannot be nil.')
      end

      if @calculation_method.nil?
        invalid_properties.push('invalid value for "calculation_method", calculation_method cannot be nil.')
      end

      if @enrollment_method.nil?
        invalid_properties.push('invalid value for "enrollment_method", enrollment_method cannot be nil.')
      end

      if @repetition.nil?
        invalid_properties.push('invalid value for "repetition", repetition cannot be nil.')
      end

      if !@avatar.nil? && @avatar.to_s.length < 1
        invalid_properties.push('invalid value for "avatar", the character length must be great than or equal to 1.')
      end

      if !@data_source.nil? && @data_source.to_s.length < 1
        invalid_properties.push('invalid value for "data_source", the character length must be great than or equal to 1.')
      end

      invalid_properties
    end

    # Check to see if the all the properties in the model are valid
    # @return true if the model is valid
    def valid?
      return false if @title.nil?
      return false if @title.to_s.length < 1
      return false if @start.nil?
      return false if @type.nil?
      type_validator = EnumAttributeValidator.new('String', ['INDIVIDUAL', 'GROUP', 'TEAM'])
      return false unless type_validator.valid?(@type)
      return false if @goal.nil?
      goal_validator = EnumAttributeValidator.new('String', ['DAILY', 'TOTAL', 'WEEKLY', 'MONTHLY'])
      return false unless goal_validator.valid?(@goal)
      return false if !@goal_value.nil? && @goal_value > 2147483647
      return false if !@goal_value.nil? && @goal_value < -2147483648
      return false if @goal_type.nil?
      goal_type_validator = EnumAttributeValidator.new('String', ['POINTS', 'DISTANCE', 'CALORIES', 'DURATION', 'STEPS'])
      return false unless goal_type_validator.valid?(@goal_type)
      return false if @visibility.nil?
      visibility_validator = EnumAttributeValidator.new('String', ['PUBLIC', 'PRIVATE'])
      return false unless visibility_validator.valid?(@visibility)
      return false if @calculation_method.nil?
      calculation_method_validator = EnumAttributeValidator.new('String', ['SUM', 'AVERAGE'])
      return false unless calculation_method_validator.valid?(@calculation_method)
      return false if @enrollment_method.nil?
      enrollment_method_validator = EnumAttributeValidator.new('String', ['AUTOMATIC', 'MANUAL'])
      return false unless enrollment_method_validator.valid?(@enrollment_method)
      default_enrollment_time_validator = EnumAttributeValidator.new('String', ['CHALLENGE_START', 'ENROLLMENT_TIME'])
      return false unless default_enrollment_time_validator.valid?(@default_enrollment_time)
      return false if @repetition.nil?
      repetition_validator = EnumAttributeValidator.new('String', ['NONE', 'STREAK', 'STICKTOIT', 'GOALBREAKER'])
      return false unless repetition_validator.valid?(@repetition)
      return false if !@avatar.nil? && @avatar.to_s.length < 1
      return false if !@data_source.nil? && @data_source.to_s.length < 1
      true
    end

    # Custom attribute writer method with validation
    # @param [Object] title Value to be assigned
    def title=(title)
      if title.nil?
        fail ArgumentError, 'title cannot be nil'
      end

      if title.to_s.length < 1
        fail ArgumentError, 'invalid value for "title", the character length must be great than or equal to 1.'
      end

      @title = title
    end

    # Custom attribute writer method checking allowed values (enum).
    # @param [Object] type Object to be assigned
    def type=(type)
      validator = EnumAttributeValidator.new('String', ['INDIVIDUAL', 'GROUP', 'TEAM'])
      unless validator.valid?(type)
        fail ArgumentError, 'invalid value for "type", must be one of #{validator.allowable_values}.'
      end
      @type = type
    end

    # Custom attribute writer method checking allowed values (enum).
    # @param [Object] goal Object to be assigned
    def goal=(goal)
      validator = EnumAttributeValidator.new('String', ['DAILY', 'TOTAL', 'WEEKLY', 'MONTHLY'])
      unless validator.valid?(goal)
        fail ArgumentError, 'invalid value for "goal", must be one of #{validator.allowable_values}.'
      end
      @goal = goal
    end

    # Custom attribute writer method with validation
    # @param [Object] goal_value Value to be assigned
    def goal_value=(goal_value)
      if !goal_value.nil? && goal_value > 2147483647
        fail ArgumentError, 'invalid value for "goal_value", must be smaller than or equal to 2147483647.'
      end

      if !goal_value.nil? && goal_value < -2147483648
        fail ArgumentError, 'invalid value for "goal_value", must be greater than or equal to -2147483648.'
      end

      @goal_value = goal_value
    end

    # Custom attribute writer method checking allowed values (enum).
    # @param [Object] goal_type Object to be assigned
    def goal_type=(goal_type)
      validator = EnumAttributeValidator.new('String', ['POINTS', 'DISTANCE', 'CALORIES', 'DURATION', 'STEPS'])
      unless validator.valid?(goal_type)
        fail ArgumentError, 'invalid value for "goal_type", must be one of #{validator.allowable_values}.'
      end
      @goal_type = goal_type
    end

    # Custom attribute writer method checking allowed values (enum).
    # @param [Object] visibility Object to be assigned
    def visibility=(visibility)
      validator = EnumAttributeValidator.new('String', ['PUBLIC', 'PRIVATE'])
      unless validator.valid?(visibility)
        fail ArgumentError, 'invalid value for "visibility", must be one of #{validator.allowable_values}.'
      end
      @visibility = visibility
    end

    # Custom attribute writer method checking allowed values (enum).
    # @param [Object] calculation_method Object to be assigned
    def calculation_method=(calculation_method)
      validator = EnumAttributeValidator.new('String', ['SUM', 'AVERAGE'])
      unless validator.valid?(calculation_method)
        fail ArgumentError, 'invalid value for "calculation_method", must be one of #{validator.allowable_values}.'
      end
      @calculation_method = calculation_method
    end

    # Custom attribute writer method checking allowed values (enum).
    # @param [Object] enrollment_method Object to be assigned
    def enrollment_method=(enrollment_method)
      validator = EnumAttributeValidator.new('String', ['AUTOMATIC', 'MANUAL'])
      unless validator.valid?(enrollment_method)
        fail ArgumentError, 'invalid value for "enrollment_method", must be one of #{validator.allowable_values}.'
      end
      @enrollment_method = enrollment_method
    end

    # Custom attribute writer method checking allowed values (enum).
    # @param [Object] default_enrollment_time Object to be assigned
    def default_enrollment_time=(default_enrollment_time)
      validator = EnumAttributeValidator.new('String', ['CHALLENGE_START', 'ENROLLMENT_TIME'])
      unless validator.valid?(default_enrollment_time)
        fail ArgumentError, 'invalid value for "default_enrollment_time", must be one of #{validator.allowable_values}.'
      end
      @default_enrollment_time = default_enrollment_time
    end

    # Custom attribute writer method checking allowed values (enum).
    # @param [Object] repetition Object to be assigned
    def repetition=(repetition)
      validator = EnumAttributeValidator.new('String', ['NONE', 'STREAK', 'STICKTOIT', 'GOALBREAKER'])
      unless validator.valid?(repetition)
        fail ArgumentError, 'invalid value for "repetition", must be one of #{validator.allowable_values}.'
      end
      @repetition = repetition
    end

    # Custom attribute writer method with validation
    # @param [Object] avatar Value to be assigned
    def avatar=(avatar)
      if !avatar.nil? && avatar.to_s.length < 1
        fail ArgumentError, 'invalid value for "avatar", the character length must be great than or equal to 1.'
      end

      @avatar = avatar
    end

    # Custom attribute writer method with validation
    # @param [Object] data_source Value to be assigned
    def data_source=(data_source)
      if !data_source.nil? && data_source.to_s.length < 1
        fail ArgumentError, 'invalid value for "data_source", the character length must be great than or equal to 1.'
      end

      @data_source = data_source
    end

    # Checks equality by comparing each attribute.
    # @param [Object] Object to be compared
    def ==(o)
      return true if self.equal?(o)
      self.class == o.class &&
          url == o.url &&
          public_id == o.public_id &&
          title == o.title &&
          slogan == o.slogan &&
          description == o.description &&
          start == o.start &&
          _end == o._end &&
          type == o.type &&
          goal == o.goal &&
          goal_value == o.goal_value &&
          goal_type == o.goal_type &&
          visibility == o.visibility &&
          num_members == o.num_members &&
          calculation_method == o.calculation_method &&
          goal_progress == o.goal_progress &&
          enrollment_method == o.enrollment_method &&
          default_enrollment_time == o.default_enrollment_time &&
          repetition == o.repetition &&
          avatar == o.avatar &&
          data_source == o.data_source &&
          activity_types == o.activity_types &&
          current_period_start == o.current_period_start &&
          current_period_end == o.current_period_end &&
          goal_cap == o.goal_cap &&
          total_calories == o.total_calories &&
          total_distance == o.total_distance &&
          total_steps == o.total_steps &&
          total_points == o.total_points
    end

    # @see the `==` method
    # @param [Object] Object to be compared
    def eql?(o)
      self == o
    end

    # Calculates hash code according to all attributes.
    # @return [Fixnum] Hash code
    def hash
      [url, public_id, title, slogan, description, start, _end, type, goal, goal_value, goal_type, visibility, num_members, calculation_method, goal_progress, enrollment_method, default_enrollment_time, repetition, avatar, data_source, activity_types, current_period_start, current_period_end, goal_cap, total_calories, total_distance, total_steps, total_points].hash
    end

    # Builds the object from hash
    # @param [Hash] attributes Model attributes in the form of hash
    # @return [Object] Returns the model itself
    def build_from_hash(attributes)
      return nil unless attributes.is_a?(Hash)
      self.class.swagger_types.each_pair do |key, type|
        if type =~ /\AArray<(.*)>/i
          # check to ensure the input is an array given that the attribute
          # is documented as an array but the input is not
          if attributes[self.class.attribute_map[key]].is_a?(Array)
            self.send("#{key}=", attributes[self.class.attribute_map[key]].map { |v| _deserialize($1, v) })
          end
        elsif !attributes[self.class.attribute_map[key]].nil?
          self.send("#{key}=", _deserialize(type, attributes[self.class.attribute_map[key]]))
        end # or else data not found in attributes(hash), not an issue as the data can be optional
      end

      self
    end

    # Deserializes the data based on type
    # @param string type Data type
    # @param string value Value to be deserialized
    # @return [Object] Deserialized data
    def _deserialize(type, value)
      case type.to_sym
      when :DateTime
        DateTime.parse(value)
      when :Date
        Date.parse(value)
      when :String
        value.to_s
      when :Integer
        value.to_i
      when :Float
        value.to_f
      when :BOOLEAN
        if value.to_s =~ /\A(true|t|yes|y|1)\z/i
          true
        else
          false
        end
      when :Object
        # generic object (usually a Hash), return directly
        value
      when /\AArray<(?<inner_type>.+)>\z/
        inner_type = Regexp.last_match[:inner_type]
        value.map { |v| _deserialize(inner_type, v) }
      when /\AHash<(?<k_type>.+?), (?<v_type>.+)>\z/
        k_type = Regexp.last_match[:k_type]
        v_type = Regexp.last_match[:v_type]
        {}.tap do |hash|
          value.each do |k, v|
            hash[_deserialize(k_type, k)] = _deserialize(v_type, v)
          end
        end
      else # model
        temp_model = WeFitter.const_get(type).new
        temp_model.build_from_hash(value)
      end
    end

    # Returns the string representation of the object
    # @return [String] String presentation of the object
    def to_s
      to_hash.to_s
    end

    # to_body is an alias to to_hash (backward compatibility)
    # @return [Hash] Returns the object in the form of hash
    def to_body
      to_hash
    end

    # Returns the object in the form of hash
    # @return [Hash] Returns the object in the form of hash
    def to_hash
      hash = {}
      self.class.attribute_map.each_pair do |attr, param|
        value = self.send(attr)
        next if value.nil?
        hash[param] = _to_hash(value)
      end
      hash
    end

    # Outputs non-array value in the form of hash
    # For object, use to_hash. Otherwise, just return the value
    # @param [Object] value Any valid value
    # @return [Hash] Returns the value in the form of hash
    def _to_hash(value)
      if value.is_a?(Array)
        value.compact.map { |v| _to_hash(v) }
      elsif value.is_a?(Hash)
        {}.tap do |hash|
          value.each { |k, v| hash[k] = _to_hash(v) }
        end
      elsif value.respond_to? :to_hash
        value.to_hash
      else
        value
      end
    end
  end
end
