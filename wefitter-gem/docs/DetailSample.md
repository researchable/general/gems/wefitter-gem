# WeFitter::DetailSample

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | **DateTime** | Starting time of the measurement. | 
**source** | **String** | Origin of the data. | 
**activity_type** | **String** |  | [optional] 
**activity_id** | **String** |  | [optional] 
**distance** | **Float** |  | [optional] 
**calories** | **Float** |  | [optional] 
**heart_rate** | **Float** |  | [optional] 


