# WeFitter::SleepSummary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | **DateTime** | Starting time of the measurement. | 
**timestamp_end** | **DateTime** |  | [optional] 
**source** | **String** | Origin of the data. | 
**is_manual** | **BOOLEAN** | Whether the data was entered manually. | [optional] 
**sleep_score** | **Integer** | Sleep score | [optional] 
**detail** | [**Array&lt;SleepDetailMinimal&gt;**](SleepDetailMinimal.md) |  | [optional] 


