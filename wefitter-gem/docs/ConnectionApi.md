# WeFitter::ConnectionApi

All URIs are relative to *https://api.wefitter.com/api/v1.3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**connection_connect_create**](ConnectionApi.md#connection_connect_create) | **POST** /connection/{connection_slug}/connect/ | Connect wearable
[**connection_connect_delete**](ConnectionApi.md#connection_connect_delete) | **DELETE** /connection/{connection_slug}/connect/ | Disconnect Wearable


# **connection_connect_create**
> AppConnectionUrl connection_connect_create(connection_slug, data)

Connect wearable

Get OAuth URL for this connection.

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ConnectionApi.new

connection_slug = 'connection_slug_example' # String | 

data = WeFitter::ProfileAsProfilePublicId.new # ProfileAsProfilePublicId | 


begin
  #Connect wearable
  result = api_instance.connection_connect_create(connection_slug, data)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ConnectionApi->connection_connect_create: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **connection_slug** | **String**|  | 
 **data** | [**ProfileAsProfilePublicId**](ProfileAsProfilePublicId.md)|  | 

### Return type

[**AppConnectionUrl**](AppConnectionUrl.md)

### Authorization

[BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **connection_connect_delete**
> connection_connect_delete(connection_slug, profile)

Disconnect Wearable

Disconnect a wearables connection

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ConnectionApi.new

connection_slug = 'connection_slug_example' # String | 

profile = 'profile_example' # String | A profile public_id is expected


begin
  #Disconnect Wearable
  api_instance.connection_connect_delete(connection_slug, profile)
rescue WeFitter::ApiError => e
  puts "Exception when calling ConnectionApi->connection_connect_delete: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **connection_slug** | **String**|  | 
 **profile** | [**String**](.md)| A profile public_id is expected | 

### Return type

nil (empty response body)

### Authorization

[BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



