# WeFitter::DailySummary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date** | **Date** | Date of the summary. | 
**distance** | **Float** | Distance in meters. | [optional] 
**steps** | **Float** | Steps taken. | [optional] 
**calories** | **Float** | Calories burned. | [optional] 
**active_calories** | **Float** | Active calorie burn. | [optional] 
**bmr_calories** | **Float** | BMR calorie burn. | [optional] 
**points** | **Float** | Points earned. | [optional] 
**source** | **String** | Origin of the data. | 
**heart_rate_summary** | [**HeartRateSummaryLinked**](HeartRateSummaryLinked.md) |  | [optional] 
**stress_summary** | [**StressSummaryLinked**](StressSummaryLinked.md) |  | [optional] 


