# WeFitter::BiometricMeasurement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | **DateTime** | Starting time of the measurement. | 
**_end** | **DateTime** | End time of the measurement. | [optional] 
**source** | **String** | Origin of the data. | 
**measurement_type** | **String** | Type of biometric measurement. | 
**is_manual** | **BOOLEAN** | Whether the data was entered manually. | [optional] 
**value** | **Float** | Value of this measurement. | [optional] 
**unit** | **String** | Unit of this measurement | [optional] 


