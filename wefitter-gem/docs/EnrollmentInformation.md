# WeFitter::EnrollmentInformation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**team** | **String** |  | [optional] 
**team_public_id** | **String** |  | [optional] 
**team_size** | **Integer** |  | [optional] 
**leaderboard_rank** | **Integer** | Current or final rank among the participants of the challenge as determined by the relevant score. | [optional] 
**leaderboard_score** | **Float** | The profile&#39;s current or final score. | [optional] 


