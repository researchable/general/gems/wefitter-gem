# WeFitter::HeartRateSummaryLinked

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**min** | **Integer** | Minimum heart rate. | [optional] 
**max** | **Integer** | Maximum heart rate. | [optional] 
**average** | **Integer** | Average heart rate. | [optional] 
**resting** | **Integer** | Resting heart rate. | [optional] 


