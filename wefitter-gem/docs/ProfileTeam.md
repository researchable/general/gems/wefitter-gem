# WeFitter::ProfileTeam

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **String** |  | [optional] 
**public_id** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**reference** | **String** |  | [optional] 


