# WeFitter::StressSummary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | **DateTime** | Starting time of the measurement. | 
**source** | **String** | Origin of the data. | 
**stress_qualifier** | **String** | Qualifier describing stress level. | 
**average_stress_level** | **Integer** | Average stress level. | [optional] 
**max_stress_level** | **Integer** | Maximum stress level. | [optional] 


