# WeFitter::Notification

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **String** |  | 
**body** | **String** |  | 
**schedule** | **DateTime** | date and time to send notification | [optional] 
**link** | **String** |  | [optional] 


