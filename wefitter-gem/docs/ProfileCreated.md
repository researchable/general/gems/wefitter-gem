# WeFitter::ProfileCreated

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **String** |  | [optional] 
**public_id** | **String** |  | [optional] 
**given_name** | **String** |  | [optional] 
**family_name** | **String** |  | [optional] 
**nickname** | **String** |  | [optional] 
**gender** | **String** |  | [optional] 
**birthdate** | **Date** |  | [optional] 
**zoneinfo** | **String** |  | [optional] 
**locale** | **String** |  | [optional] 
**reference** | **String** |  | 
**avatar** | **String** |  | [optional] 
**bearer** | **String** | Used for authenticating as a profile. Make this the active Authorization bearer to perform actions using this profile. | [optional] 


