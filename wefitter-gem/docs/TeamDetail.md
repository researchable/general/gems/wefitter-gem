# WeFitter::TeamDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**public_id** | **String** |  | [optional] 
**url** | **String** |  | [optional] 
**num_members** | **String** |  | [optional] 
**members** | [**Array&lt;ProfileTeam&gt;**](ProfileTeam.md) | List of members in this team | [optional] 
**avatar** | **String** |  | [optional] 
**description** | **String** |  | 


