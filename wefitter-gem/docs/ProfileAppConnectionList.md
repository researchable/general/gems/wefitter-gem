# WeFitter::ProfileAppConnectionList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**display_name** | **String** |  | [optional] 
**url** | **String** |  | [optional] 
**connected** | **String** |  | [optional] 
**connection_slug** | **String** |  | [optional] 
**img_url** | **String** |  | [optional] 
**created_at** | **DateTime** |  | [optional] 
**updated_at** | **DateTime** |  | [optional] 


