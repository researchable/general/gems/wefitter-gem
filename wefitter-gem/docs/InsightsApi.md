# WeFitter::InsightsApi

All URIs are relative to *https://api.wefitter.com/api/v1.3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**insights_bio_age_read**](InsightsApi.md#insights_bio_age_read) | **GET** /insights/bio_age | Get someones biological age on a weekly basis.


# **insights_bio_age_read**
> Array&lt;BioAge&gt; insights_bio_age_read(start_date, end_date)

Get someones biological age on a weekly basis.



### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::InsightsApi.new

start_date = Date.parse('2013-10-20') # Date | 

end_date = Date.parse('2013-10-20') # Date | 


begin
  #Get someones biological age on a weekly basis.
  result = api_instance.insights_bio_age_read(start_date, end_date)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling InsightsApi->insights_bio_age_read: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start_date** | **Date**|  | 
 **end_date** | **Date**|  | 

### Return type

[**Array&lt;BioAge&gt;**](BioAge.md)

### Authorization

[BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



