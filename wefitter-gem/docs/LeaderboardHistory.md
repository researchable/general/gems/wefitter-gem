# WeFitter::LeaderboardHistory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**score** | **Float** |  | [optional] 
**start** | **DateTime** |  | [optional] 
**_end** | **DateTime** |  | [optional] 


