# WeFitter::ProfileAsProfilePublicId

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**profile** | **String** | A profile public_id is expected | 
**joined** | **DateTime** | The timestamp when the profile/team joined the challenge. | [optional] 


