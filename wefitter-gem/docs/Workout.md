# WeFitter::Workout

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | **DateTime** | Starting time of the measurement. | 
**source** | **String** | Origin of the data. | 
**is_manual** | **BOOLEAN** | Whether the data was entered manually. | [optional] 
**type** | **String** | Type of the activity. | 
**distance** | **Float** | Distance in meters. | [optional] 
**steps** | **Float** | Steps taken. | [optional] 
**elevation** | **Float** | Elevation gained during the workout. | [optional] 
**speed** | **Float** | Average speed during the workout | [optional] 
**calories** | **Float** | Calories burned. | [optional] 
**points** | **Float** | Points earned. | [optional] 
**in_outdoor** | **String** | in- or outdoor-activity. | [optional] 
**heart_rate_summary** | [**HeartRateSummaryLinked**](HeartRateSummaryLinked.md) |  | [optional] 


