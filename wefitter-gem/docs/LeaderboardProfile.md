# WeFitter::LeaderboardProfile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **String** |  | [optional] 
**public_id** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**gender** | **String** |  | [optional] 
**teams** | [**Array&lt;TeamProfile&gt;**](TeamProfile.md) | Team of which this profile is a member of | [optional] 
**reference** | **String** |  | [optional] 
**score** | **Float** |  | [optional] 
**position** | **Integer** |  | [optional] 
**avatar** | **String** |  | 


