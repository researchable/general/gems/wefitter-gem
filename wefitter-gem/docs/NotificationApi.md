# WeFitter::NotificationApi

All URIs are relative to *https://api.wefitter.com/api/v1.3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**notification_send_to_all_create**](NotificationApi.md#notification_send_to_all_create) | **POST** /notification/send_to_all/ | Notification to all profiles in app
[**notification_send_to_profiles_create**](NotificationApi.md#notification_send_to_profiles_create) | **POST** /notification/send_to_profiles/ | Notification to specific profiles


# **notification_send_to_all_create**
> Notification notification_send_to_all_create(data)

Notification to all profiles in app

Send a notification to all registered devices in the app.

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::NotificationApi.new

data = WeFitter::Notification.new # Notification | 


begin
  #Notification to all profiles in app
  result = api_instance.notification_send_to_all_create(data)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling NotificationApi->notification_send_to_all_create: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**Notification**](Notification.md)|  | 

### Return type

[**Notification**](Notification.md)

### Authorization

[BearerAdmin](../README.md#BearerAdmin)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **notification_send_to_profiles_create**
> NotificationForProfiles notification_send_to_profiles_create(data)

Notification to specific profiles

Send a notification to all registered devices for the specified profiles.

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::NotificationApi.new

data = WeFitter::NotificationForProfiles.new # NotificationForProfiles | 


begin
  #Notification to specific profiles
  result = api_instance.notification_send_to_profiles_create(data)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling NotificationApi->notification_send_to_profiles_create: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**NotificationForProfiles**](NotificationForProfiles.md)|  | 

### Return type

[**NotificationForProfiles**](NotificationForProfiles.md)

### Authorization

[BearerAdmin](../README.md#BearerAdmin)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



