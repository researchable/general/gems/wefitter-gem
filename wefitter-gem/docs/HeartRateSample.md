# WeFitter::HeartRateSample

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | **DateTime** | Starting time of the measurement. | 
**source** | **String** | Origin of the data. | 
**is_manual** | **BOOLEAN** | Whether the data was entered manually. | [optional] 
**value** | **Integer** | Measured heart rate in beats/min. | [optional] 
**min** | **Integer** | Minimum value measured in the time window. | [optional] 
**max** | **Integer** | Maximum value measured in the time window. | [optional] 


