# WeFitter::StressSummaryLinked

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stress_qualifier** | **String** | Qualifier describing stress level. | 
**average_stress_level** | **Integer** | Average stress level. | [optional] 
**max_stress_level** | **Integer** | Maximum stress level. | [optional] 


