# WeFitter::ProfileAsProfilePublicIdAndTeamAsTeamPublicId

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**profile** | **String** | A profile public_id is expected | 
**team** | **String** | A team public_id is expected | [optional] 
**is_active** | **BOOLEAN** | Whether profile actively participates in challenge.  While inactive, scores/leaderboard will not be updated but still be visible. | [optional] 
**joined** | **DateTime** | The timestamp when the profile/team joined the challenge. | [optional] 


