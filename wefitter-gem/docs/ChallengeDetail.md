# WeFitter::ChallengeDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **String** |  | [optional] 
**public_id** | **String** |  | [optional] 
**title** | **String** |  | 
**slogan** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**start** | **DateTime** |  | 
**_end** | **DateTime** |  | [optional] 
**type** | **String** |  | 
**goal** | **String** |  | 
**goal_value** | **Integer** |  | [optional] 
**goal_type** | **String** |  | 
**visibility** | **String** |  | 
**num_members** | **String** |  | [optional] 
**calculation_method** | **String** |  | 
**goal_progress** | **Float** | Progress towards the goal, from perspective of profile whose bearer is used for authentication: can be an individual/team/global score. When goal is duration, this is the duration in seconds. When an admin token is used, returns 0.0. | [optional] 
**enrollment_method** | **String** |  | 
**default_enrollment_time** | **String** |  | [optional] 
**repetition** | **String** |  | 
**avatar** | **String** |  | [optional] 
**data_source** | **String** |  | [optional] 
**activity_types** | [**Array&lt;ChallengeActivityType&gt;**](ChallengeActivityType.md) |  | [optional] 
**current_period_start** | **DateTime** | The start (exclusive) of the current period. Null if the challenge has not started yet. | [optional] 
**current_period_end** | **DateTime** | The end (exclusive) of the current period. Null if the challenge end has passed. | [optional] 
**goal_cap** | **BOOLEAN** | Whether scores should be unable to surpass the goal value. | [optional] 
**total_calories** | **Float** |  | [optional] 
**total_distance** | **Float** |  | [optional] 
**total_steps** | **Float** |  | [optional] 
**total_points** | **Float** |  | [optional] 


