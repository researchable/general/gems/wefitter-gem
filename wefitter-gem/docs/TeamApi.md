# WeFitter::TeamApi

All URIs are relative to *https://api.wefitter.com/api/v1.3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**team_create**](TeamApi.md#team_create) | **POST** /team/ | Create Team
[**team_delete**](TeamApi.md#team_delete) | **DELETE** /team/{public_id}/ | Deletes a Team
[**team_list**](TeamApi.md#team_list) | **GET** /team/ | List Teams
[**team_member_create**](TeamApi.md#team_member_create) | **POST** /team/{public_id}/member/ | Add member
[**team_member_delete**](TeamApi.md#team_member_delete) | **DELETE** /team/{public_id}/member/ | Remove member
[**team_member_update**](TeamApi.md#team_member_update) | **PUT** /team/{public_id}/member/ | Add member
[**team_members_create**](TeamApi.md#team_members_create) | **POST** /team/{public_id}/members/ | Add members
[**team_members_update**](TeamApi.md#team_members_update) | **PUT** /team/{public_id}/members/ | Add members
[**team_partial_update**](TeamApi.md#team_partial_update) | **PATCH** /team/{public_id}/ | Partial Edit a Team
[**team_read**](TeamApi.md#team_read) | **GET** /team/{public_id}/ | Team
[**team_remove_members**](TeamApi.md#team_remove_members) | **POST** /team/{public_id}/remove_members/ | Remove members
[**team_update**](TeamApi.md#team_update) | **PUT** /team/{public_id}/ | Edit Team


# **team_create**
> TeamDetail team_create(data)

Create Team

Creates a new team

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'

  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::TeamApi.new

data = WeFitter::TeamDetail.new # TeamDetail | 


begin
  #Create Team
  result = api_instance.team_create(data)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling TeamApi->team_create: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**TeamDetail**](TeamDetail.md)|  | 

### Return type

[**TeamDetail**](TeamDetail.md)

### Authorization

[BearerAdmin](../README.md#BearerAdmin), [BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **team_delete**
> team_delete(public_id, )

Deletes a Team

Deletes an existing team

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'

  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::TeamApi.new

public_id = 'public_id_example' # String | 


begin
  #Deletes a Team
  api_instance.team_delete(public_id, )
rescue WeFitter::ApiError => e
  puts "Exception when calling TeamApi->team_delete: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **public_id** | **String**|  | 

### Return type

nil (empty response body)

### Authorization

[BearerAdmin](../README.md#BearerAdmin), [BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **team_list**
> InlineResponse2005 team_list(opts)

List Teams

List all available teams

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'

  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::TeamApi.new

opts = { 
  limit: 56, # Integer | Number of results to return per page.
  offset: 56 # Integer | The initial index from which to return the results.
}

begin
  #List Teams
  result = api_instance.team_list(opts)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling TeamApi->team_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **Integer**| Number of results to return per page. | [optional] 
 **offset** | **Integer**| The initial index from which to return the results. | [optional] 

### Return type

[**InlineResponse2005**](InlineResponse2005.md)

### Authorization

[BearerAdmin](../README.md#BearerAdmin), [BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **team_member_create**
> ProfileAsProfilePublicId team_member_create(public_id, data)

Add member

Add member to a team

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'

  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::TeamApi.new

public_id = 'public_id_example' # String | 

data = WeFitter::ProfileAsProfilePublicId.new # ProfileAsProfilePublicId | 


begin
  #Add member
  result = api_instance.team_member_create(public_id, data)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling TeamApi->team_member_create: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **public_id** | **String**|  | 
 **data** | [**ProfileAsProfilePublicId**](ProfileAsProfilePublicId.md)|  | 

### Return type

[**ProfileAsProfilePublicId**](ProfileAsProfilePublicId.md)

### Authorization

[BearerAdmin](../README.md#BearerAdmin), [BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **team_member_delete**
> team_member_delete(public_id, profile)

Remove member

Remove a member from the team

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'

  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::TeamApi.new

public_id = 'public_id_example' # String | 

profile = 'profile_example' # String | A profile public_id is expected


begin
  #Remove member
  api_instance.team_member_delete(public_id, profile)
rescue WeFitter::ApiError => e
  puts "Exception when calling TeamApi->team_member_delete: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **public_id** | **String**|  | 
 **profile** | [**String**](.md)| A profile public_id is expected | 

### Return type

nil (empty response body)

### Authorization

[BearerAdmin](../README.md#BearerAdmin), [BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **team_member_update**
> ProfileAsProfilePublicId team_member_update(public_id, data)

Add member

Add member to a team

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'

  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::TeamApi.new

public_id = 'public_id_example' # String | 

data = WeFitter::ProfileAsProfilePublicId.new # ProfileAsProfilePublicId | 


begin
  #Add member
  result = api_instance.team_member_update(public_id, data)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling TeamApi->team_member_update: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **public_id** | **String**|  | 
 **data** | [**ProfileAsProfilePublicId**](ProfileAsProfilePublicId.md)|  | 

### Return type

[**ProfileAsProfilePublicId**](ProfileAsProfilePublicId.md)

### Authorization

[BearerAdmin](../README.md#BearerAdmin), [BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **team_members_create**
> ProfilePublicIdsList team_members_create(public_id, data)

Add members

Add members to a team

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::TeamApi.new

public_id = 'public_id_example' # String | 

data = WeFitter::ProfilePublicIdsList.new # ProfilePublicIdsList | 


begin
  #Add members
  result = api_instance.team_members_create(public_id, data)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling TeamApi->team_members_create: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **public_id** | **String**|  | 
 **data** | [**ProfilePublicIdsList**](ProfilePublicIdsList.md)|  | 

### Return type

[**ProfilePublicIdsList**](ProfilePublicIdsList.md)

### Authorization

[BearerAdmin](../README.md#BearerAdmin)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **team_members_update**
> ProfilePublicIdsList team_members_update(public_id, data)

Add members

Add members to a team

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::TeamApi.new

public_id = 'public_id_example' # String | 

data = WeFitter::ProfilePublicIdsList.new # ProfilePublicIdsList | 


begin
  #Add members
  result = api_instance.team_members_update(public_id, data)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling TeamApi->team_members_update: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **public_id** | **String**|  | 
 **data** | [**ProfilePublicIdsList**](ProfilePublicIdsList.md)|  | 

### Return type

[**ProfilePublicIdsList**](ProfilePublicIdsList.md)

### Authorization

[BearerAdmin](../README.md#BearerAdmin)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **team_partial_update**
> TeamDetail team_partial_update(public_id, data)

Partial Edit a Team

Creates a new team

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'

  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::TeamApi.new

public_id = 'public_id_example' # String | 

data = WeFitter::TeamDetail.new # TeamDetail | 


begin
  #Partial Edit a Team
  result = api_instance.team_partial_update(public_id, data)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling TeamApi->team_partial_update: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **public_id** | **String**|  | 
 **data** | [**TeamDetail**](TeamDetail.md)|  | 

### Return type

[**TeamDetail**](TeamDetail.md)

### Authorization

[BearerAdmin](../README.md#BearerAdmin), [BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **team_read**
> TeamDetail team_read(public_id, )

Team

Get a single team

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'

  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::TeamApi.new

public_id = 'public_id_example' # String | 


begin
  #Team
  result = api_instance.team_read(public_id, )
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling TeamApi->team_read: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **public_id** | **String**|  | 

### Return type

[**TeamDetail**](TeamDetail.md)

### Authorization

[BearerAdmin](../README.md#BearerAdmin), [BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **team_remove_members**
> ProfilePublicIdsList team_remove_members(public_id, data)

Remove members

Remove members from the team

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::TeamApi.new

public_id = 'public_id_example' # String | 

data = WeFitter::ProfilePublicIdsList.new # ProfilePublicIdsList | 


begin
  #Remove members
  result = api_instance.team_remove_members(public_id, data)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling TeamApi->team_remove_members: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **public_id** | **String**|  | 
 **data** | [**ProfilePublicIdsList**](ProfilePublicIdsList.md)|  | 

### Return type

[**ProfilePublicIdsList**](ProfilePublicIdsList.md)

### Authorization

[BearerAdmin](../README.md#BearerAdmin)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **team_update**
> TeamDetail team_update(public_id, data)

Edit Team

Creates a new team

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'

  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::TeamApi.new

public_id = 'public_id_example' # String | 

data = WeFitter::TeamDetail.new # TeamDetail | 


begin
  #Edit Team
  result = api_instance.team_update(public_id, data)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling TeamApi->team_update: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **public_id** | **String**|  | 
 **data** | [**TeamDetail**](TeamDetail.md)|  | 

### Return type

[**TeamDetail**](TeamDetail.md)

### Authorization

[BearerAdmin](../README.md#BearerAdmin), [BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



