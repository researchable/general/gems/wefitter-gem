# WeFitter::LoyaltyApi

All URIs are relative to *https://api.wefitter.com/api/v1.3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**loyalty_get_recommended_products_list**](LoyaltyApi.md#loyalty_get_recommended_products_list) | **GET** /loyalty/get_recommended_products | 
[**loyalty_get_shoppingcart_link_read**](LoyaltyApi.md#loyalty_get_shoppingcart_link_read) | **GET** /loyalty/get_shoppingcart_link | 


# **loyalty_get_recommended_products_list**
> InlineResponse2003 loyalty_get_recommended_products_list(opts)





### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::LoyaltyApi.new

opts = { 
  limit: 56, # Integer | Number of results to return per page.
  offset: 56 # Integer | The initial index from which to return the results.
}

begin
  result = api_instance.loyalty_get_recommended_products_list(opts)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling LoyaltyApi->loyalty_get_recommended_products_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **Integer**| Number of results to return per page. | [optional] 
 **offset** | **Integer**| The initial index from which to return the results. | [optional] 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **loyalty_get_shoppingcart_link_read**
> ShoppingCart loyalty_get_shoppingcart_link_read





### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::LoyaltyApi.new

begin
  result = api_instance.loyalty_get_shoppingcart_link_read
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling LoyaltyApi->loyalty_get_shoppingcart_link_read: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ShoppingCart**](ShoppingCart.md)

### Authorization

[BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



