# WeFitter::ECGRecordings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | **DateTime** | Starting time of the measurement. | 
**duration** | **String** | Duration of the measurement. | [optional] 
**source** | **String** | Origin of the data. | 
**sample_frequency** | **Integer** | Sampling frequency of heart beats as integer in Hz. | [optional] 
**sample_count** | **Integer** | Sample count of heart beats as integer. | [optional] 


