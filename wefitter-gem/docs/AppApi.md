# WeFitter::AppApi

All URIs are relative to *https://api.wefitter.com/api/v1.3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**app_totals_list**](AppApi.md#app_totals_list) | **GET** /app/totals/ | Totals


# **app_totals_list**
> Array&lt;AppTotals&gt; app_totals_list

Totals

Total values for calories, distance, steps, points and activity duration over all profiles in the app.

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'

  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::AppApi.new

begin
  #Totals
  result = api_instance.app_totals_list
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling AppApi->app_totals_list: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Array&lt;AppTotals&gt;**](AppTotals.md)

### Authorization

[BearerAdmin](../README.md#BearerAdmin), [BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



