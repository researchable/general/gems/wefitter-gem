# WeFitter::HeartRateSummary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | **DateTime** | Starting time of the measurement. | 
**source** | **String** | Origin of the data. | 
**is_manual** | **BOOLEAN** | Whether the data was entered manually. | [optional] 
**min** | **Integer** | Minimum heart rate. | [optional] 
**max** | **Integer** | Maximum heart rate. | [optional] 
**average** | **Integer** | Average heart rate. | [optional] 
**resting** | **Integer** | Resting heart rate. | [optional] 


