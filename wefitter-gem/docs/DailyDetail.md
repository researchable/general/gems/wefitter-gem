# WeFitter::DailyDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | **DateTime** | Starting time of the measurement. | 
**duration** | **String** | Duration of the measurement. | [optional] 
**source** | **String** | Origin of the data. | 
**steps** | **Integer** | Steps taken. | [optional] 
**calories** | **Float** | Calories burned. | [optional] 
**distance** | **Float** | Distance in meters. | [optional] 


