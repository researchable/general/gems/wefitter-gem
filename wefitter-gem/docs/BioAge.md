# WeFitter::BioAge

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start** | **Date** |  | 
**_end** | **Date** |  | 
**bio_age** | **Float** |  | 


