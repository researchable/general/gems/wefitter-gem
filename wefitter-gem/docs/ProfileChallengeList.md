# WeFitter::ProfileChallengeList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **String** |  | [optional] 
**public_id** | **String** |  | [optional] 
**title** | **String** |  | 
**slogan** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**start** | **DateTime** |  | 
**_end** | **DateTime** |  | [optional] 
**type** | **String** |  | 
**goal** | **String** |  | 
**goal_value** | **Integer** |  | [optional] 
**goal_type** | **String** |  | 
**visibility** | **String** |  | 
**num_members** | **String** |  | [optional] 
**leaderboard_rank** | **Integer** | Current or final rank among the participants of the challenge as determined by the relevant score. | [optional] 
**leaderboard_score** | **Float** | The profile&#39;s current or final score. | [optional] 
**is_participating** | **BOOLEAN** | Does the current profile participate in this challenge? | [optional] 
**enrollment** | [**EnrollmentInformation**](EnrollmentInformation.md) |  | [optional] 
**repetition** | **String** |  | 
**avatar** | **String** |  | [optional] 
**data_source** | **String** |  | [optional] 
**activity_types** | [**Array&lt;ChallengeActivityType&gt;**](ChallengeActivityType.md) |  | [optional] 
**current_period_start** | **DateTime** | The start (exclusive) of the current period. Null if the challenge has not started yet. | [optional] 
**current_period_end** | **DateTime** | The end (exclusive) of the current period. Null if the challenge end has passed. | [optional] 
**goal_cap** | **BOOLEAN** | Whether scores should be unable to surpass the goal value. | [optional] 
**joined** | **DateTime** | The timestamp when the profile joined the challenge. | [optional] 


