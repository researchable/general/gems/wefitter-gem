# WeFitter::AppTotals

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total_calories** | **Float** |  | [optional] 
**total_distance** | **Float** |  | [optional] 
**total_steps** | **Float** |  | [optional] 
**total_points** | **Float** |  | [optional] 


