# WeFitter::ChallengeApi

All URIs are relative to *https://api.wefitter.com/api/v1.3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**challenge_create**](ChallengeApi.md#challenge_create) | **POST** /challenge/ | Create challenge
[**challenge_deactivate_member_create**](ChallengeApi.md#challenge_deactivate_member_create) | **POST** /challenge/{public_id}/deactivate_member/ | Deactivate member
[**challenge_deactivate_member_update**](ChallengeApi.md#challenge_deactivate_member_update) | **PUT** /challenge/{public_id}/deactivate_member/ | 
[**challenge_deactivate_members_create**](ChallengeApi.md#challenge_deactivate_members_create) | **POST** /challenge/{public_id}/deactivate_members/ | Deactivate members
[**challenge_deactivate_members_update**](ChallengeApi.md#challenge_deactivate_members_update) | **PUT** /challenge/{public_id}/deactivate_members/ | 
[**challenge_delete**](ChallengeApi.md#challenge_delete) | **DELETE** /challenge/{public_id}/ | Delete challenge
[**challenge_leaderboard**](ChallengeApi.md#challenge_leaderboard) | **GET** /challenge/{public_id}/leaderboard/ | Leaderboard
[**challenge_leaderboard_history**](ChallengeApi.md#challenge_leaderboard_history) | **GET** /challenge/{public_id}/leaderboard_history/ | Leaderboard history
[**challenge_list**](ChallengeApi.md#challenge_list) | **GET** /challenge/ | List Challenges
[**challenge_member_create**](ChallengeApi.md#challenge_member_create) | **POST** /challenge/{public_id}/member/ | Add member
[**challenge_member_delete**](ChallengeApi.md#challenge_member_delete) | **DELETE** /challenge/{public_id}/member/ | Remove member
[**challenge_member_update**](ChallengeApi.md#challenge_member_update) | **PUT** /challenge/{public_id}/member/ | Add member
[**challenge_members_create**](ChallengeApi.md#challenge_members_create) | **POST** /challenge/{public_id}/members/ | Add members
[**challenge_members_read**](ChallengeApi.md#challenge_members_read) | **GET** /challenge/{public_id}/members/ | Get members
[**challenge_members_update**](ChallengeApi.md#challenge_members_update) | **PUT** /challenge/{public_id}/members/ | Add members
[**challenge_partial_update**](ChallengeApi.md#challenge_partial_update) | **PATCH** /challenge/{public_id}/ | Partial edit challenge
[**challenge_periods**](ChallengeApi.md#challenge_periods) | **GET** /challenge/{public_id}/periods/ | Periods
[**challenge_read**](ChallengeApi.md#challenge_read) | **GET** /challenge/{public_id}/ | Get Challenge
[**challenge_remove_members**](ChallengeApi.md#challenge_remove_members) | **POST** /challenge/{public_id}/remove_members/ | Remove members
[**challenge_team_list**](ChallengeApi.md#challenge_team_list) | **GET** /challenge/{challenge_public_id}/team/ | Team leaderboard
[**challenge_team_read**](ChallengeApi.md#challenge_team_read) | **GET** /challenge/{challenge_public_id}/team/{team_public_id}/ | Team contribution
[**challenge_update**](ChallengeApi.md#challenge_update) | **PUT** /challenge/{public_id}/ | Edit challenge


# **challenge_create**
> ChallengeDetail challenge_create(data)

Create challenge

Creates a new challenge

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ChallengeApi.new

data = WeFitter::ChallengeDetail.new # ChallengeDetail | 


begin
  #Create challenge
  result = api_instance.challenge_create(data)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ChallengeApi->challenge_create: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**ChallengeDetail**](ChallengeDetail.md)|  | 

### Return type

[**ChallengeDetail**](ChallengeDetail.md)

### Authorization

[BearerAdmin](../README.md#BearerAdmin)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **challenge_deactivate_member_create**
> ProfileAsProfilePublicIdAndTeamAsTeamPublicId challenge_deactivate_member_create(public_id, data, profile, opts)

Deactivate member

Deactivate a member from the challenge

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'

  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ChallengeApi.new

public_id = 'public_id_example' # String | 

data = WeFitter::ProfileAsProfilePublicIdAndTeamAsTeamPublicId.new # ProfileAsProfilePublicIdAndTeamAsTeamPublicId | 

profile = 'profile_example' # String | A profile public_id is expected

opts = { 
  team: 'team_example', # String | A team public_id is expected
  is_active: true # BOOLEAN | Whether profile actively participates in challenge.  While inactive, scores/leaderboard will not be updated but still be visible.
}

begin
  #Deactivate member
  result = api_instance.challenge_deactivate_member_create(public_id, data, profile, opts)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ChallengeApi->challenge_deactivate_member_create: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **public_id** | **String**|  | 
 **data** | [**ProfileAsProfilePublicIdAndTeamAsTeamPublicId**](ProfileAsProfilePublicIdAndTeamAsTeamPublicId.md)|  | 
 **profile** | [**String**](.md)| A profile public_id is expected | 
 **team** | [**String**](.md)| A team public_id is expected | [optional] 
 **is_active** | **BOOLEAN**| Whether profile actively participates in challenge.  While inactive, scores/leaderboard will not be updated but still be visible. | [optional] 

### Return type

[**ProfileAsProfilePublicIdAndTeamAsTeamPublicId**](ProfileAsProfilePublicIdAndTeamAsTeamPublicId.md)

### Authorization

[BearerAdmin](../README.md#BearerAdmin), [BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **challenge_deactivate_member_update**
> ProfileAsProfilePublicIdAndTeamAsTeamPublicId challenge_deactivate_member_update(public_id, data)



This docstring is used by Django Rest Framework

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ChallengeApi.new

public_id = 'public_id_example' # String | 

data = WeFitter::ProfileAsProfilePublicIdAndTeamAsTeamPublicId.new # ProfileAsProfilePublicIdAndTeamAsTeamPublicId | 


begin
  result = api_instance.challenge_deactivate_member_update(public_id, data)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ChallengeApi->challenge_deactivate_member_update: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **public_id** | **String**|  | 
 **data** | [**ProfileAsProfilePublicIdAndTeamAsTeamPublicId**](ProfileAsProfilePublicIdAndTeamAsTeamPublicId.md)|  | 

### Return type

[**ProfileAsProfilePublicIdAndTeamAsTeamPublicId**](ProfileAsProfilePublicIdAndTeamAsTeamPublicId.md)

### Authorization

[BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **challenge_deactivate_members_create**
> ProfilePublicIdsListAndTeamAsPublicId challenge_deactivate_members_create(public_id, data, profile, opts)

Deactivate members

Deactivate multiple members from the challenge

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'

  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ChallengeApi.new

public_id = 'public_id_example' # String | 

data = WeFitter::ProfilePublicIdsListAndTeamAsPublicId.new # ProfilePublicIdsListAndTeamAsPublicId | 

profile = 'profile_example' # String | A profile public_id is expected

opts = { 
  team: 'team_example', # String | A team public_id is expected
  is_active: true # BOOLEAN | Whether profile actively participates in challenge.  While inactive, scores/leaderboard will not be updated but still be visible.
}

begin
  #Deactivate members
  result = api_instance.challenge_deactivate_members_create(public_id, data, profile, opts)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ChallengeApi->challenge_deactivate_members_create: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **public_id** | **String**|  | 
 **data** | [**ProfilePublicIdsListAndTeamAsPublicId**](ProfilePublicIdsListAndTeamAsPublicId.md)|  | 
 **profile** | [**String**](.md)| A profile public_id is expected | 
 **team** | [**String**](.md)| A team public_id is expected | [optional] 
 **is_active** | **BOOLEAN**| Whether profile actively participates in challenge.  While inactive, scores/leaderboard will not be updated but still be visible. | [optional] 

### Return type

[**ProfilePublicIdsListAndTeamAsPublicId**](ProfilePublicIdsListAndTeamAsPublicId.md)

### Authorization

[BearerAdmin](../README.md#BearerAdmin), [BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **challenge_deactivate_members_update**
> ProfilePublicIdsListAndTeamAsPublicId challenge_deactivate_members_update(public_id, data)



This docstring is used by Django Rest Framework

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ChallengeApi.new

public_id = 'public_id_example' # String | 

data = WeFitter::ProfilePublicIdsListAndTeamAsPublicId.new # ProfilePublicIdsListAndTeamAsPublicId | 


begin
  result = api_instance.challenge_deactivate_members_update(public_id, data)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ChallengeApi->challenge_deactivate_members_update: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **public_id** | **String**|  | 
 **data** | [**ProfilePublicIdsListAndTeamAsPublicId**](ProfilePublicIdsListAndTeamAsPublicId.md)|  | 

### Return type

[**ProfilePublicIdsListAndTeamAsPublicId**](ProfilePublicIdsListAndTeamAsPublicId.md)

### Authorization

[BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **challenge_delete**
> challenge_delete(public_id, )

Delete challenge

deletes a challenge

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ChallengeApi.new

public_id = 'public_id_example' # String | 


begin
  #Delete challenge
  api_instance.challenge_delete(public_id, )
rescue WeFitter::ApiError => e
  puts "Exception when calling ChallengeApi->challenge_delete: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **public_id** | **String**|  | 

### Return type

nil (empty response body)

### Authorization

[BearerAdmin](../README.md#BearerAdmin)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **challenge_leaderboard**
> LeaderboardProfileAndTeam challenge_leaderboard(public_id, opts)

Leaderboard

Displays the leaderboard for a challenge. This endpoint returns a list of objects. 

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'

  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ChallengeApi.new

public_id = 'public_id_example' # String | 

opts = { 
  date_range: 56 # Integer | Index of the time period use
}

begin
  #Leaderboard
  result = api_instance.challenge_leaderboard(public_id, opts)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ChallengeApi->challenge_leaderboard: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **public_id** | **String**|  | 
 **date_range** | **Integer**| Index of the time period use | [optional] 

### Return type

[**LeaderboardProfileAndTeam**](LeaderboardProfileAndTeam.md)

### Authorization

[BearerAdmin](../README.md#BearerAdmin), [BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **challenge_leaderboard_history**
> LeaderboardHistory challenge_leaderboard_history(public_id)

Leaderboard history

Retrieve the scores of the authenticated profile for all periods since the profile joined the challenge. This endpoint returns a list of objects. 

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ChallengeApi.new

public_id = 'public_id_example' # String | 


begin
  #Leaderboard history
  result = api_instance.challenge_leaderboard_history(public_id)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ChallengeApi->challenge_leaderboard_history: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **public_id** | **String**|  | 

### Return type

[**LeaderboardHistory**](LeaderboardHistory.md)

### Authorization

[BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **challenge_list**
> InlineResponse200 challenge_list(opts)

List Challenges

Lists all available challenges

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'

  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ChallengeApi.new

opts = { 
  limit: 56, # Integer | Number of results to return per page.
  offset: 56, # Integer | The initial index from which to return the results.
  recent: 56 # Integer | Show challenges that have not ended in the last N weeks
}

begin
  #List Challenges
  result = api_instance.challenge_list(opts)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ChallengeApi->challenge_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **Integer**| Number of results to return per page. | [optional] 
 **offset** | **Integer**| The initial index from which to return the results. | [optional] 
 **recent** | **Integer**| Show challenges that have not ended in the last N weeks | [optional] 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

[BearerAdmin](../README.md#BearerAdmin), [BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **challenge_member_create**
> ProfileAsProfilePublicIdAndTeamAsTeamPublicId challenge_member_create(public_id, data)

Add member

Add member to a team.  Note: for now if the challenge is a team challenge and no team is specified, the first team for the specified profile is used. Do note that this is a temporary fix and may be removed at any time. 

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'

  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ChallengeApi.new

public_id = 'public_id_example' # String | 

data = WeFitter::ProfileAsProfilePublicIdAndTeamAsTeamPublicId.new # ProfileAsProfilePublicIdAndTeamAsTeamPublicId | 


begin
  #Add member
  result = api_instance.challenge_member_create(public_id, data)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ChallengeApi->challenge_member_create: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **public_id** | **String**|  | 
 **data** | [**ProfileAsProfilePublicIdAndTeamAsTeamPublicId**](ProfileAsProfilePublicIdAndTeamAsTeamPublicId.md)|  | 

### Return type

[**ProfileAsProfilePublicIdAndTeamAsTeamPublicId**](ProfileAsProfilePublicIdAndTeamAsTeamPublicId.md)

### Authorization

[BearerAdmin](../README.md#BearerAdmin), [BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **challenge_member_delete**
> challenge_member_delete(public_id, profile)

Remove member

Remove a member from the challenge

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'

  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ChallengeApi.new

public_id = 'public_id_example' # String | 

profile = 'profile_example' # String | A profile public_id is expected


begin
  #Remove member
  api_instance.challenge_member_delete(public_id, profile)
rescue WeFitter::ApiError => e
  puts "Exception when calling ChallengeApi->challenge_member_delete: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **public_id** | **String**|  | 
 **profile** | [**String**](.md)| A profile public_id is expected | 

### Return type

nil (empty response body)

### Authorization

[BearerAdmin](../README.md#BearerAdmin), [BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **challenge_member_update**
> ProfileAsProfilePublicIdAndTeamAsTeamPublicId challenge_member_update(public_id, data)

Add member

Add member to a team.  Note: for now if the challenge is a team challenge and no team is specified, the first team for the specified profile is used. Do note that this is a temporary fix and may be removed at any time. 

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'

  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ChallengeApi.new

public_id = 'public_id_example' # String | 

data = WeFitter::ProfileAsProfilePublicIdAndTeamAsTeamPublicId.new # ProfileAsProfilePublicIdAndTeamAsTeamPublicId | 


begin
  #Add member
  result = api_instance.challenge_member_update(public_id, data)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ChallengeApi->challenge_member_update: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **public_id** | **String**|  | 
 **data** | [**ProfileAsProfilePublicIdAndTeamAsTeamPublicId**](ProfileAsProfilePublicIdAndTeamAsTeamPublicId.md)|  | 

### Return type

[**ProfileAsProfilePublicIdAndTeamAsTeamPublicId**](ProfileAsProfilePublicIdAndTeamAsTeamPublicId.md)

### Authorization

[BearerAdmin](../README.md#BearerAdmin), [BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **challenge_members_create**
> ProfilePublicIdsListAndTeamAsPublicId challenge_members_create(public_id, data)

Add members

Add members to a team.  Note: for now if the challenge is a team challenge and no team is specified, the first team for the specified profile is used. Do note that this is a temporary fix and may be removed at any time.  Note: for the GET/POST endpoint an admin bearer token is required, while the GET also accepts profile bearer tokens

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ChallengeApi.new

public_id = 'public_id_example' # String | 

data = WeFitter::ProfilePublicIdsListAndTeamAsPublicId.new # ProfilePublicIdsListAndTeamAsPublicId | 


begin
  #Add members
  result = api_instance.challenge_members_create(public_id, data)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ChallengeApi->challenge_members_create: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **public_id** | **String**|  | 
 **data** | [**ProfilePublicIdsListAndTeamAsPublicId**](ProfilePublicIdsListAndTeamAsPublicId.md)|  | 

### Return type

[**ProfilePublicIdsListAndTeamAsPublicId**](ProfilePublicIdsListAndTeamAsPublicId.md)

### Authorization

[BearerAdmin](../README.md#BearerAdmin)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **challenge_members_read**
> InlineResponse2002 challenge_members_read(public_id, , opts)

Get members

Get a paginated list of members enrolled to a Challenge

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'

  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ChallengeApi.new

public_id = 'public_id_example' # String | 

opts = { 
  limit: 56, # Integer | Number of results to return per page.
  offset: 56 # Integer | The initial index from which to return the results.
}

begin
  #Get members
  result = api_instance.challenge_members_read(public_id, , opts)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ChallengeApi->challenge_members_read: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **public_id** | **String**|  | 
 **limit** | **Integer**| Number of results to return per page. | [optional] 
 **offset** | **Integer**| The initial index from which to return the results. | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[BearerAdmin](../README.md#BearerAdmin), [BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **challenge_members_update**
> ProfilePublicIdsListAndTeamAsPublicId challenge_members_update(public_id, data)

Add members

Add members to a team.  Note: for now if the challenge is a team challenge and no team is specified, the first team for the specified profile is used. Do note that this is a temporary fix and may be removed at any time.  Note: for the GET/POST endpoint an admin bearer token is required, while the GET also accepts profile bearer tokens

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ChallengeApi.new

public_id = 'public_id_example' # String | 

data = WeFitter::ProfilePublicIdsListAndTeamAsPublicId.new # ProfilePublicIdsListAndTeamAsPublicId | 


begin
  #Add members
  result = api_instance.challenge_members_update(public_id, data)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ChallengeApi->challenge_members_update: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **public_id** | **String**|  | 
 **data** | [**ProfilePublicIdsListAndTeamAsPublicId**](ProfilePublicIdsListAndTeamAsPublicId.md)|  | 

### Return type

[**ProfilePublicIdsListAndTeamAsPublicId**](ProfilePublicIdsListAndTeamAsPublicId.md)

### Authorization

[BearerAdmin](../README.md#BearerAdmin)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **challenge_partial_update**
> ChallengeDetail challenge_partial_update(public_id, data)

Partial edit challenge

Edits a challenge

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ChallengeApi.new

public_id = 'public_id_example' # String | 

data = WeFitter::ChallengeDetail.new # ChallengeDetail | 


begin
  #Partial edit challenge
  result = api_instance.challenge_partial_update(public_id, data)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ChallengeApi->challenge_partial_update: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **public_id** | **String**|  | 
 **data** | [**ChallengeDetail**](ChallengeDetail.md)|  | 

### Return type

[**ChallengeDetail**](ChallengeDetail.md)

### Authorization

[BearerAdmin](../README.md#BearerAdmin)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **challenge_periods**
> ChallengePeriod challenge_periods(public_id)

Periods

Get all periods for the challenge. This endpoint returns a list of objects. 

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'

  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ChallengeApi.new

public_id = 'public_id_example' # String | 


begin
  #Periods
  result = api_instance.challenge_periods(public_id)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ChallengeApi->challenge_periods: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **public_id** | **String**|  | 

### Return type

[**ChallengePeriod**](ChallengePeriod.md)

### Authorization

[BearerAdmin](../README.md#BearerAdmin), [BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **challenge_read**
> ChallengeDetail challenge_read(public_id, )

Get Challenge

Gets a single challenge

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'

  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ChallengeApi.new

public_id = 'public_id_example' # String | 


begin
  #Get Challenge
  result = api_instance.challenge_read(public_id, )
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ChallengeApi->challenge_read: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **public_id** | **String**|  | 

### Return type

[**ChallengeDetail**](ChallengeDetail.md)

### Authorization

[BearerAdmin](../README.md#BearerAdmin), [BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **challenge_remove_members**
> ProfilePublicIdsListAndTeamAsPublicId challenge_remove_members(public_id, data)

Remove members

Remove members from the challenge

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ChallengeApi.new

public_id = 'public_id_example' # String | 

data = WeFitter::ProfilePublicIdsListAndTeamAsPublicId.new # ProfilePublicIdsListAndTeamAsPublicId | 


begin
  #Remove members
  result = api_instance.challenge_remove_members(public_id, data)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ChallengeApi->challenge_remove_members: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **public_id** | **String**|  | 
 **data** | [**ProfilePublicIdsListAndTeamAsPublicId**](ProfilePublicIdsListAndTeamAsPublicId.md)|  | 

### Return type

[**ProfilePublicIdsListAndTeamAsPublicId**](ProfilePublicIdsListAndTeamAsPublicId.md)

### Authorization

[BearerAdmin](../README.md#BearerAdmin)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **challenge_team_list**
> InlineResponse2001 challenge_team_list(challenge_public_id, opts)

Team leaderboard

List the standings of different teams in challenges (don't have to be team challenges). This endpoint returns a list of objects. 

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'

  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ChallengeApi.new

challenge_public_id = 'challenge_public_id_example' # String | 

opts = { 
  limit: 56, # Integer | Number of results to return per page.
  offset: 56, # Integer | The initial index from which to return the results.
  date_range: 56 # Integer | Index of the time period use
}

begin
  #Team leaderboard
  result = api_instance.challenge_team_list(challenge_public_id, opts)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ChallengeApi->challenge_team_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **challenge_public_id** | **String**|  | 
 **limit** | **Integer**| Number of results to return per page. | [optional] 
 **offset** | **Integer**| The initial index from which to return the results. | [optional] 
 **date_range** | **Integer**| Index of the time period use | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[BearerAdmin](../README.md#BearerAdmin), [BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **challenge_team_read**
> LeaderboardProfile challenge_team_read(challenge_public_id, team_public_id, opts)

Team contribution

List team members' contribution to the challenge. This endpoint returns a list of objects. 

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'

  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ChallengeApi.new

challenge_public_id = 'challenge_public_id_example' # String | 

team_public_id = 'team_public_id_example' # String | 

opts = { 
  date_range: 56 # Integer | Index of the time period use
}

begin
  #Team contribution
  result = api_instance.challenge_team_read(challenge_public_id, team_public_id, opts)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ChallengeApi->challenge_team_read: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **challenge_public_id** | **String**|  | 
 **team_public_id** | **String**|  | 
 **date_range** | **Integer**| Index of the time period use | [optional] 

### Return type

[**LeaderboardProfile**](LeaderboardProfile.md)

### Authorization

[BearerAdmin](../README.md#BearerAdmin), [BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **challenge_update**
> ChallengeDetail challenge_update(public_id, data)

Edit challenge

Edits a challenge

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ChallengeApi.new

public_id = 'public_id_example' # String | 

data = WeFitter::ChallengeDetail.new # ChallengeDetail | 


begin
  #Edit challenge
  result = api_instance.challenge_update(public_id, data)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ChallengeApi->challenge_update: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **public_id** | **String**|  | 
 **data** | [**ChallengeDetail**](ChallengeDetail.md)|  | 

### Return type

[**ChallengeDetail**](ChallengeDetail.md)

### Authorization

[BearerAdmin](../README.md#BearerAdmin)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



