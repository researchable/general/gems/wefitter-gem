# WeFitter::TokenApi

All URIs are relative to *https://api.wefitter.com/api/v1.3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**token_create**](TokenApi.md#token_create) | **POST** /token/ | Create Token


# **token_create**
> Token token_create(data)

Create Token

 In order to create a token, perform a POST request to this endpoint, example:<br/> <table><tbody>     <tr><td>Application ID</td><td>123</td></tr>     <tr><td>Application Secret</td><td>ABC</td></tr>     <tr><td>Authorization header format</td><td>Authorization: base64(app_id:app_secret)</td></tr>     <tr><td>Expected result</td><td>Authorization: MTIzOkFCQw==</td></tr> </tbody></table> For testing purposes, a CURL can be used: <pre> curl -X POST -H \"Authorization: Basic MTIzOkFCQw==\" https://api.wefitter.com/api/v1.3/token/ # alternatively curl -X POST --user 123:ABC https://api.wefitter.com/api/v1.3/token/ </pre>  The response to a successful request should look like this: <pre> {     \"bearer\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOiIxNTY2NTYxMzk2IiwiaWF0IjoxNTE2MjM5MDIyfQ.14x12h5VMJjAV2kFB5cCrfFmZhryBAYGk9N-vhyDl00\" } </pre> 

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure HTTP basic authorization: Basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = WeFitter::TokenApi.new

data = WeFitter::Token.new # Token | 


begin
  #Create Token
  result = api_instance.token_create(data)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling TokenApi->token_create: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**Token**](Token.md)|  | 

### Return type

[**Token**](Token.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



