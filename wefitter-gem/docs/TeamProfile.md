# WeFitter::TeamProfile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**public_id** | **String** |  | [optional] 
**url** | **String** |  | [optional] 
**avatar** | **String** |  | [optional] 


