# WeFitter::ProfilePublicIdsList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**profiles** | **Array&lt;String&gt;** |  | 
**is_active** | **BOOLEAN** | Whether profile actively participates in challenge.  While inactive, scores/leaderboard will not be updated but still be visible. | [optional] 
**joined** | **DateTime** | The timestamp when the profile/team joined the challenge. | [optional] 


