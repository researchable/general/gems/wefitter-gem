# WeFitter::LeaderboardTeam

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**public_id** | **String** |  | [optional] 
**url** | **String** |  | [optional] 
**num_members** | **String** |  | [optional] 
**score** | **Float** |  | [optional] 
**position** | **Integer** |  | [optional] 
**avatar** | **String** |  | 


