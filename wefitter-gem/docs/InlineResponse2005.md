# WeFitter::InlineResponse2005

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  | 
**_next** | **String** |  | [optional] 
**previous** | **String** |  | [optional] 
**results** | [**Array&lt;TeamList&gt;**](TeamList.md) |  | 


