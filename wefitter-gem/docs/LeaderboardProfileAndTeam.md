# WeFitter::LeaderboardProfileAndTeam

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**public_id** | **String** |  | [optional] 
**url** | **String** |  | [optional] 
**score** | **Float** |  | [optional] 
**position** | **Integer** |  | [optional] 
**avatar** | **String** |  | 


