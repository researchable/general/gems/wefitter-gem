# WeFitter::ProfileApi

All URIs are relative to *https://api.wefitter.com/api/v1.3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**profile_activity_segment_list**](ProfileApi.md#profile_activity_segment_list) | **GET** /profile/{profile_public_id}/activity_segment/ | Activity segment
[**profile_biometric_list**](ProfileApi.md#profile_biometric_list) | **GET** /profile/{profile_public_id}/biometric/ | Bio Metric
[**profile_biometric_measurement_list**](ProfileApi.md#profile_biometric_measurement_list) | **GET** /profile/{profile_public_id}/biometric_measurement/ | Biometric Measurement
[**profile_bmi_list**](ProfileApi.md#profile_bmi_list) | **GET** /profile/{profile_public_id}/bmi/ | BMI
[**profile_challenge**](ProfileApi.md#profile_challenge) | **GET** /profile/{public_id}/challenge/ | Challenge
[**profile_challenges**](ProfileApi.md#profile_challenges) | **GET** /profile/{public_id}/challenges/ | 
[**profile_connections**](ProfileApi.md#profile_connections) | **GET** /profile/{public_id}/connections/ | Connections
[**profile_create**](ProfileApi.md#profile_create) | **POST** /profile/ | Create Profile
[**profile_daily_detail_list**](ProfileApi.md#profile_daily_detail_list) | **GET** /profile/{profile_public_id}/daily_detail/ | Daily details
[**profile_daily_summary_list**](ProfileApi.md#profile_daily_summary_list) | **GET** /profile/{profile_public_id}/daily_summary/ | Daily Summary
[**profile_delete**](ProfileApi.md#profile_delete) | **DELETE** /profile/{public_id}/ | Delete Profile
[**profile_detail_sample_list**](ProfileApi.md#profile_detail_sample_list) | **GET** /profile/{profile_public_id}/detail_sample/ | Detail Sample
[**profile_ecg_recordings_data_list**](ProfileApi.md#profile_ecg_recordings_data_list) | **GET** /profile/{profile_public_id}/ecg_recordings/data/ | Electrocardiogram data
[**profile_ecg_recordings_list**](ProfileApi.md#profile_ecg_recordings_list) | **GET** /profile/{profile_public_id}/ecg_recordings/ | Electrocardiograms
[**profile_ecg_recordings_pdf_list**](ProfileApi.md#profile_ecg_recordings_pdf_list) | **GET** /profile/{profile_public_id}/ecg_recordings/pdf/ | Electrocardiogram PDF
[**profile_event_create**](ProfileApi.md#profile_event_create) | **POST** /profile/{profile_public_id}/event/ | Create Event
[**profile_event_list**](ProfileApi.md#profile_event_list) | **GET** /profile/{profile_public_id}/event/ | Event
[**profile_heartrate_sample_list**](ProfileApi.md#profile_heartrate_sample_list) | **GET** /profile/{profile_public_id}/heartrate_sample/ | Heart Rate Sample
[**profile_heartrate_summary_list**](ProfileApi.md#profile_heartrate_summary_list) | **GET** /profile/{profile_public_id}/heartrate_summary/ | Heart Rate Summary
[**profile_height_list**](ProfileApi.md#profile_height_list) | **GET** /profile/{profile_public_id}/height/ | Height
[**profile_list**](ProfileApi.md#profile_list) | **GET** /profile/ | List Profiles
[**profile_partial_update**](ProfileApi.md#profile_partial_update) | **PATCH** /profile/{public_id}/ | Partial Update
[**profile_read**](ProfileApi.md#profile_read) | **GET** /profile/{public_id}/ | Get Profile
[**profile_sleep_detail_list**](ProfileApi.md#profile_sleep_detail_list) | **GET** /profile/{profile_public_id}/sleep_detail/ | Sleep detail
[**profile_sleep_summary_list**](ProfileApi.md#profile_sleep_summary_list) | **GET** /profile/{profile_public_id}/sleep_summary/ | Sleep Summary
[**profile_stress_samples_list**](ProfileApi.md#profile_stress_samples_list) | **GET** /profile/{profile_public_id}/stress_samples/ | Stress Sample
[**profile_stress_summary_list**](ProfileApi.md#profile_stress_summary_list) | **GET** /profile/{profile_public_id}/stress_summary/ | Stress Summary
[**profile_track_sample_list**](ProfileApi.md#profile_track_sample_list) | **GET** /profile/{profile_public_id}/track_sample/ | Track Sample
[**profile_update**](ProfileApi.md#profile_update) | **PUT** /profile/{public_id}/ | Update profile
[**profile_weight_list**](ProfileApi.md#profile_weight_list) | **GET** /profile/{profile_public_id}/weight/ | Weight
[**profile_workout_list**](ProfileApi.md#profile_workout_list) | **GET** /profile/{profile_public_id}/workout/ | Workout


# **profile_activity_segment_list**
> Array&lt;Activity&gt; profile_activity_segment_list(profile_public_id, opts)

Activity segment

Lists segments of activities and small non-workout-like movements.

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ProfileApi.new

profile_public_id = 'profile_public_id_example' # String | 

opts = { 
  cursor: 'cursor_example', # String | The pagination cursor value.
  page_size: 56, # Integer | Number of results to return per page.
  date_start: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Lower bound of date or timestamp.
  date_end: DateTime.parse('2013-10-20T19:20:30+01:00') # DateTime | Upper bound of date or timestamp.
}

begin
  #Activity segment
  result = api_instance.profile_activity_segment_list(profile_public_id, opts)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ProfileApi->profile_activity_segment_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profile_public_id** | **String**|  | 
 **cursor** | **String**| The pagination cursor value. | [optional] 
 **page_size** | **Integer**| Number of results to return per page. | [optional] 
 **date_start** | **DateTime**| Lower bound of date or timestamp. | [optional] 
 **date_end** | **DateTime**| Upper bound of date or timestamp. | [optional] 

### Return type

[**Array&lt;Activity&gt;**](Activity.md)

### Authorization

[BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **profile_biometric_list**
> Array&lt;BioMetric&gt; profile_biometric_list(profile_public_id, opts)

Bio Metric

Lists changes in weight, height and bmi for the profile.

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ProfileApi.new

profile_public_id = 'profile_public_id_example' # String | 

opts = { 
  cursor: 'cursor_example', # String | The pagination cursor value.
  page_size: 56, # Integer | Number of results to return per page.
  date_start: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Lower bound of date or timestamp.
  date_end: DateTime.parse('2013-10-20T19:20:30+01:00') # DateTime | Upper bound of date or timestamp.
}

begin
  #Bio Metric
  result = api_instance.profile_biometric_list(profile_public_id, opts)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ProfileApi->profile_biometric_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profile_public_id** | **String**|  | 
 **cursor** | **String**| The pagination cursor value. | [optional] 
 **page_size** | **Integer**| Number of results to return per page. | [optional] 
 **date_start** | **DateTime**| Lower bound of date or timestamp. | [optional] 
 **date_end** | **DateTime**| Upper bound of date or timestamp. | [optional] 

### Return type

[**Array&lt;BioMetric&gt;**](BioMetric.md)

### Authorization

[BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **profile_biometric_measurement_list**
> Array&lt;BiometricMeasurement&gt; profile_biometric_measurement_list(profile_public_id, measurement_type, opts)

Biometric Measurement

List changes in biometric measurements for the profile.

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ProfileApi.new

profile_public_id = 'profile_public_id_example' # String | 

measurement_type = 'measurement_type_example' # String | 

opts = { 
  cursor: 'cursor_example', # String | The pagination cursor value.
  page_size: 56, # Integer | Number of results to return per page.
  date_start: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Lower bound of date or timestamp.
  date_end: DateTime.parse('2013-10-20T19:20:30+01:00') # DateTime | Upper bound of date or timestamp.
}

begin
  #Biometric Measurement
  result = api_instance.profile_biometric_measurement_list(profile_public_id, measurement_type, opts)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ProfileApi->profile_biometric_measurement_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profile_public_id** | **String**|  | 
 **measurement_type** | **String**|  | 
 **cursor** | **String**| The pagination cursor value. | [optional] 
 **page_size** | **Integer**| Number of results to return per page. | [optional] 
 **date_start** | **DateTime**| Lower bound of date or timestamp. | [optional] 
 **date_end** | **DateTime**| Upper bound of date or timestamp. | [optional] 

### Return type

[**Array&lt;BiometricMeasurement&gt;**](BiometricMeasurement.md)

### Authorization

[BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **profile_bmi_list**
> Array&lt;BMI&gt; profile_bmi_list(profile_public_id, opts)

BMI

Presents the Body-Mass-Index over time.

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ProfileApi.new

profile_public_id = 'profile_public_id_example' # String | 

opts = { 
  cursor: 'cursor_example', # String | The pagination cursor value.
  page_size: 56, # Integer | Number of results to return per page.
  date_start: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Lower bound of date or timestamp.
  date_end: DateTime.parse('2013-10-20T19:20:30+01:00') # DateTime | Upper bound of date or timestamp.
}

begin
  #BMI
  result = api_instance.profile_bmi_list(profile_public_id, opts)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ProfileApi->profile_bmi_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profile_public_id** | **String**|  | 
 **cursor** | **String**| The pagination cursor value. | [optional] 
 **page_size** | **Integer**| Number of results to return per page. | [optional] 
 **date_start** | **DateTime**| Lower bound of date or timestamp. | [optional] 
 **date_end** | **DateTime**| Upper bound of date or timestamp. | [optional] 

### Return type

[**Array&lt;BMI&gt;**](BMI.md)

### Authorization

[BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **profile_challenge**
> ProfileChallengeList profile_challenge(public_id, opts)

Challenge

Provides a list of challenges where the profile can participate and is already participating in.  This endpoint returns a list of objects. 

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ProfileApi.new

public_id = 'public_id_example' # String | 

opts = { 
  recent: 56, # Integer | Show challenges that have not ended in the last N weeks
  date_start: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Only return challenges that are active after this time  Note: this only affects challenges that are not joined.  Defaults to now
  date_end: DateTime.parse('2013-10-20T19:20:30+01:00') # DateTime | Only return challenges that are active before this time  Note: this only affects challenges that are not joined.  Defaults to now
}

begin
  #Challenge
  result = api_instance.profile_challenge(public_id, opts)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ProfileApi->profile_challenge: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **public_id** | **String**|  | 
 **recent** | **Integer**| Show challenges that have not ended in the last N weeks | [optional] 
 **date_start** | **DateTime**| Only return challenges that are active after this time  Note: this only affects challenges that are not joined.  Defaults to now | [optional] 
 **date_end** | **DateTime**| Only return challenges that are active before this time  Note: this only affects challenges that are not joined.  Defaults to now | [optional] 

### Return type

[**ProfileChallengeList**](ProfileChallengeList.md)

### Authorization

[BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **profile_challenges**
> ProfileChallengeList profile_challenges(public_id)



This docstring is used by Django Rest Framework

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ProfileApi.new

public_id = 'public_id_example' # String | 


begin
  result = api_instance.profile_challenges(public_id)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ProfileApi->profile_challenges: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **public_id** | **String**|  | 

### Return type

[**ProfileChallengeList**](ProfileChallengeList.md)

### Authorization

[BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **profile_connections**
> ProfileAppConnectionList profile_connections(public_id, opts)

Connections

Lists the connections available for a profile and their \"state\". This endpoint returns a list of objects. 

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ProfileApi.new

public_id = 'public_id_example' # String | 

opts = { 
  redirect: 'redirect_example', # String | After connecting redirect back to this url.  Note: has to be a valid http url. For deeplinks (to apps) first redirect to your own backend and then deeplink from there.  Note: the user is only redirected on a successfull connection, otherwise an error screen is shown. (see redirect_on_error to change this)  Note: if this parameter is omitted, then user will see a generic success screen.
  redirect_on_error: true # BOOLEAN | If enabled, then user will also be redirected to the redirect url if an error occurs  Note: a query parameter 'error' is added to the redirect url. Possible values are access_denied, unknown. New values may be added at anytime  Note: for backwards compatibility the default is false, but this will be changed in the next breaking release
}

begin
  #Connections
  result = api_instance.profile_connections(public_id, opts)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ProfileApi->profile_connections: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **public_id** | **String**|  | 
 **redirect** | **String**| After connecting redirect back to this url.  Note: has to be a valid http url. For deeplinks (to apps) first redirect to your own backend and then deeplink from there.  Note: the user is only redirected on a successfull connection, otherwise an error screen is shown. (see redirect_on_error to change this)  Note: if this parameter is omitted, then user will see a generic success screen. | [optional] 
 **redirect_on_error** | **BOOLEAN**| If enabled, then user will also be redirected to the redirect url if an error occurs  Note: a query parameter &#39;error&#39; is added to the redirect url. Possible values are access_denied, unknown. New values may be added at anytime  Note: for backwards compatibility the default is false, but this will be changed in the next breaking release | [optional] 

### Return type

[**ProfileAppConnectionList**](ProfileAppConnectionList.md)

### Authorization

[BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **profile_create**
> ProfileCreated profile_create(data)

Create Profile

 Creates a new profile. The bearer field contains a profile bearer token. When this is used in the Authorization header actions are performed as that profile, which will also enable access to private data. Using a profile bearer token disables administrative features such as creating profiles, making this token safe for 'public' use. A profile bearer token has no expiration, so it remains valid as long as the profile is active. 

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ProfileApi.new

data = WeFitter::ProfileCreated.new # ProfileCreated | 


begin
  #Create Profile
  result = api_instance.profile_create(data)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ProfileApi->profile_create: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**ProfileCreated**](ProfileCreated.md)|  | 

### Return type

[**ProfileCreated**](ProfileCreated.md)

### Authorization

[BearerAdmin](../README.md#BearerAdmin)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **profile_daily_detail_list**
> Array&lt;DailyDetail&gt; profile_daily_detail_list(profile_public_id, opts)

Daily details

Intraday data measured at a single point in time.

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ProfileApi.new

profile_public_id = 'profile_public_id_example' # String | 

opts = { 
  cursor: 'cursor_example', # String | The pagination cursor value.
  page_size: 56, # Integer | Number of results to return per page.
  date_start: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Lower bound of date or timestamp.
  date_end: DateTime.parse('2013-10-20T19:20:30+01:00') # DateTime | Upper bound of date or timestamp.
}

begin
  #Daily details
  result = api_instance.profile_daily_detail_list(profile_public_id, opts)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ProfileApi->profile_daily_detail_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profile_public_id** | **String**|  | 
 **cursor** | **String**| The pagination cursor value. | [optional] 
 **page_size** | **Integer**| Number of results to return per page. | [optional] 
 **date_start** | **DateTime**| Lower bound of date or timestamp. | [optional] 
 **date_end** | **DateTime**| Upper bound of date or timestamp. | [optional] 

### Return type

[**Array&lt;DailyDetail&gt;**](DailyDetail.md)

### Authorization

[BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **profile_daily_summary_list**
> Array&lt;DailySummary&gt; profile_daily_summary_list(profile_public_id, opts)

Daily Summary

Provides summarized data per day.

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ProfileApi.new

profile_public_id = 'profile_public_id_example' # String | 

opts = { 
  cursor: 'cursor_example', # String | The pagination cursor value.
  page_size: 56, # Integer | Number of results to return per page.
  date_start: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Lower bound of date or timestamp.
  date_end: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Upper bound of date or timestamp.
  deduplicate: true # BOOLEAN | If set to true, duplicates will be aggregated.
}

begin
  #Daily Summary
  result = api_instance.profile_daily_summary_list(profile_public_id, opts)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ProfileApi->profile_daily_summary_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profile_public_id** | **String**|  | 
 **cursor** | **String**| The pagination cursor value. | [optional] 
 **page_size** | **Integer**| Number of results to return per page. | [optional] 
 **date_start** | **DateTime**| Lower bound of date or timestamp. | [optional] 
 **date_end** | **DateTime**| Upper bound of date or timestamp. | [optional] 
 **deduplicate** | **BOOLEAN**| If set to true, duplicates will be aggregated. | [optional] 

### Return type

[**Array&lt;DailySummary&gt;**](DailySummary.md)

### Authorization

[BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **profile_delete**
> profile_delete(public_id, )

Delete Profile

Deletes the specified profile, if created by the app.

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'

  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ProfileApi.new

public_id = 'public_id_example' # String | 


begin
  #Delete Profile
  api_instance.profile_delete(public_id, )
rescue WeFitter::ApiError => e
  puts "Exception when calling ProfileApi->profile_delete: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **public_id** | **String**|  | 

### Return type

nil (empty response body)

### Authorization

[BearerAdmin](../README.md#BearerAdmin), [BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **profile_detail_sample_list**
> Array&lt;DetailSample&gt; profile_detail_sample_list(profile_public_id, opts)

Detail Sample

Presents detail samples.

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ProfileApi.new

profile_public_id = 'profile_public_id_example' # String | 

opts = { 
  cursor: 'cursor_example', # String | The pagination cursor value.
  page_size: 56, # Integer | Number of results to return per page.
  date_start: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Lower bound of date or timestamp.
  date_end: DateTime.parse('2013-10-20T19:20:30+01:00') # DateTime | Upper bound of date or timestamp.
}

begin
  #Detail Sample
  result = api_instance.profile_detail_sample_list(profile_public_id, opts)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ProfileApi->profile_detail_sample_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profile_public_id** | **String**|  | 
 **cursor** | **String**| The pagination cursor value. | [optional] 
 **page_size** | **Integer**| Number of results to return per page. | [optional] 
 **date_start** | **DateTime**| Lower bound of date or timestamp. | [optional] 
 **date_end** | **DateTime**| Upper bound of date or timestamp. | [optional] 

### Return type

[**Array&lt;DetailSample&gt;**](DetailSample.md)

### Authorization

[BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **profile_ecg_recordings_data_list**
> Array&lt;ECGRecording&gt; profile_ecg_recordings_data_list(profile_public_id, opts)

Electrocardiogram data

 This endpoint returns an ECG recording with data as an array of tuples `[(time, value)]`. Set the argument `date_end` to request a specific recording or leave it empty to receive the latest recording. 

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ProfileApi.new

profile_public_id = 'profile_public_id_example' # String | 

opts = { 
  cursor: 'cursor_example', # String | The pagination cursor value.
  page_size: 56, # Integer | Number of results to return per page.
  date_start: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Lower bound of date or timestamp.
  date_end: DateTime.parse('2013-10-20T19:20:30+01:00') # DateTime | Upper bound of date or timestamp.
}

begin
  #Electrocardiogram data
  result = api_instance.profile_ecg_recordings_data_list(profile_public_id, opts)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ProfileApi->profile_ecg_recordings_data_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profile_public_id** | **String**|  | 
 **cursor** | **String**| The pagination cursor value. | [optional] 
 **page_size** | **Integer**| Number of results to return per page. | [optional] 
 **date_start** | **DateTime**| Lower bound of date or timestamp. | [optional] 
 **date_end** | **DateTime**| Upper bound of date or timestamp. | [optional] 

### Return type

[**Array&lt;ECGRecording&gt;**](ECGRecording.md)

### Authorization

[BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **profile_ecg_recordings_list**
> Array&lt;ECGRecordings&gt; profile_ecg_recordings_list(profile_public_id, opts)

Electrocardiograms

 This endpoint returns a list of ECG recordings. 

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ProfileApi.new

profile_public_id = 'profile_public_id_example' # String | 

opts = { 
  cursor: 'cursor_example', # String | The pagination cursor value.
  page_size: 56, # Integer | Number of results to return per page.
  date_start: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Lower bound of date or timestamp.
  date_end: DateTime.parse('2013-10-20T19:20:30+01:00') # DateTime | Upper bound of date or timestamp.
}

begin
  #Electrocardiograms
  result = api_instance.profile_ecg_recordings_list(profile_public_id, opts)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ProfileApi->profile_ecg_recordings_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profile_public_id** | **String**|  | 
 **cursor** | **String**| The pagination cursor value. | [optional] 
 **page_size** | **Integer**| Number of results to return per page. | [optional] 
 **date_start** | **DateTime**| Lower bound of date or timestamp. | [optional] 
 **date_end** | **DateTime**| Upper bound of date or timestamp. | [optional] 

### Return type

[**Array&lt;ECGRecordings&gt;**](ECGRecordings.md)

### Authorization

[BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **profile_ecg_recordings_pdf_list**
> profile_ecg_recordings_pdf_list(profile_public_id, datetime, source)

Electrocardiogram PDF

Returns a PDF with generated ECG charts from a specific source and time. Contact api-support@wefitter.com to gain access.

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ProfileApi.new

profile_public_id = 'profile_public_id_example' # String | 

datetime = DateTime.parse('2013-10-20T19:20:30+01:00') # DateTime | The timestamp when the ECG has been recorded in the format `yyyymmddhhmmss`. e.g. `20210913132513`

source = 'source_example' # String | 


begin
  #Electrocardiogram PDF
  api_instance.profile_ecg_recordings_pdf_list(profile_public_id, datetime, source)
rescue WeFitter::ApiError => e
  puts "Exception when calling ProfileApi->profile_ecg_recordings_pdf_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profile_public_id** | **String**|  | 
 **datetime** | **DateTime**| The timestamp when the ECG has been recorded in the format &#x60;yyyymmddhhmmss&#x60;. e.g. &#x60;20210913132513&#x60; | 
 **source** | **String**|  | 

### Return type

nil (empty response body)

### Authorization

[BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **profile_event_create**
> Event profile_event_create(profile_public_id, data)

Create Event

Create a new event for the profile.

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ProfileApi.new

profile_public_id = 'profile_public_id_example' # String | 

data = WeFitter::Event.new # Event | 


begin
  #Create Event
  result = api_instance.profile_event_create(profile_public_id, data)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ProfileApi->profile_event_create: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profile_public_id** | **String**|  | 
 **data** | [**Event**](Event.md)|  | 

### Return type

[**Event**](Event.md)

### Authorization

[BearerAdmin](../README.md#BearerAdmin)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **profile_event_list**
> Array&lt;Event&gt; profile_event_list(profile_public_id, , opts)

Event

Lists events, generic actions for which points have been awarded.

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ProfileApi.new

profile_public_id = 'profile_public_id_example' # String | 

opts = { 
  cursor: 'cursor_example', # String | The pagination cursor value.
  page_size: 56, # Integer | Number of results to return per page.
  date_start: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Lower bound of date or timestamp.
  date_end: DateTime.parse('2013-10-20T19:20:30+01:00') # DateTime | Upper bound of date or timestamp.
}

begin
  #Event
  result = api_instance.profile_event_list(profile_public_id, , opts)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ProfileApi->profile_event_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profile_public_id** | **String**|  | 
 **cursor** | **String**| The pagination cursor value. | [optional] 
 **page_size** | **Integer**| Number of results to return per page. | [optional] 
 **date_start** | **DateTime**| Lower bound of date or timestamp. | [optional] 
 **date_end** | **DateTime**| Upper bound of date or timestamp. | [optional] 

### Return type

[**Array&lt;Event&gt;**](Event.md)

### Authorization

[BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **profile_heartrate_sample_list**
> Array&lt;HeartRateSample&gt; profile_heartrate_sample_list(profile_public_id, opts)

Heart Rate Sample

Heart Rate measured at a single point in time.

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ProfileApi.new

profile_public_id = 'profile_public_id_example' # String | 

opts = { 
  cursor: 'cursor_example', # String | The pagination cursor value.
  page_size: 56, # Integer | Number of results to return per page.
  date_start: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Lower bound of date or timestamp.
  date_end: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Upper bound of date or timestamp.
  heart_rate_detail: true # BOOLEAN | If set to true it will add heart_rate_samples field
}

begin
  #Heart Rate Sample
  result = api_instance.profile_heartrate_sample_list(profile_public_id, opts)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ProfileApi->profile_heartrate_sample_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profile_public_id** | **String**|  | 
 **cursor** | **String**| The pagination cursor value. | [optional] 
 **page_size** | **Integer**| Number of results to return per page. | [optional] 
 **date_start** | **DateTime**| Lower bound of date or timestamp. | [optional] 
 **date_end** | **DateTime**| Upper bound of date or timestamp. | [optional] 
 **heart_rate_detail** | **BOOLEAN**| If set to true it will add heart_rate_samples field | [optional] 

### Return type

[**Array&lt;HeartRateSample&gt;**](HeartRateSample.md)

### Authorization

[BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **profile_heartrate_summary_list**
> Array&lt;HeartRateSummary&gt; profile_heartrate_summary_list(profile_public_id, opts)

Heart Rate Summary

Presents heart rate information.

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ProfileApi.new

profile_public_id = 'profile_public_id_example' # String | 

opts = { 
  cursor: 'cursor_example', # String | The pagination cursor value.
  page_size: 56, # Integer | Number of results to return per page.
  date_start: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Lower bound of date or timestamp.
  date_end: DateTime.parse('2013-10-20T19:20:30+01:00') # DateTime | Upper bound of date or timestamp.
}

begin
  #Heart Rate Summary
  result = api_instance.profile_heartrate_summary_list(profile_public_id, opts)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ProfileApi->profile_heartrate_summary_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profile_public_id** | **String**|  | 
 **cursor** | **String**| The pagination cursor value. | [optional] 
 **page_size** | **Integer**| Number of results to return per page. | [optional] 
 **date_start** | **DateTime**| Lower bound of date or timestamp. | [optional] 
 **date_end** | **DateTime**| Upper bound of date or timestamp. | [optional] 

### Return type

[**Array&lt;HeartRateSummary&gt;**](HeartRateSummary.md)

### Authorization

[BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **profile_height_list**
> Array&lt;Height&gt; profile_height_list(profile_public_id, opts)

Height

Lists changes in height for the profile.

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ProfileApi.new

profile_public_id = 'profile_public_id_example' # String | 

opts = { 
  cursor: 'cursor_example', # String | The pagination cursor value.
  page_size: 56, # Integer | Number of results to return per page.
  date_start: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Lower bound of date or timestamp.
  date_end: DateTime.parse('2013-10-20T19:20:30+01:00') # DateTime | Upper bound of date or timestamp.
}

begin
  #Height
  result = api_instance.profile_height_list(profile_public_id, opts)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ProfileApi->profile_height_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profile_public_id** | **String**|  | 
 **cursor** | **String**| The pagination cursor value. | [optional] 
 **page_size** | **Integer**| Number of results to return per page. | [optional] 
 **date_start** | **DateTime**| Lower bound of date or timestamp. | [optional] 
 **date_end** | **DateTime**| Upper bound of date or timestamp. | [optional] 

### Return type

[**Array&lt;Height&gt;**](Height.md)

### Authorization

[BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **profile_list**
> InlineResponse2004 profile_list(opts)

List Profiles

Lists all available profiles

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerAdmin
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'

  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ProfileApi.new

opts = { 
  search: 'search_example', # String | A search term.
  ordering: 'ordering_example', # String | Which field to use when ordering the results.
  limit: 56, # Integer | Number of results to return per page.
  offset: 56 # Integer | The initial index from which to return the results.
}

begin
  #List Profiles
  result = api_instance.profile_list(opts)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ProfileApi->profile_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search** | **String**| A search term. | [optional] 
 **ordering** | **String**| Which field to use when ordering the results. | [optional] 
 **limit** | **Integer**| Number of results to return per page. | [optional] 
 **offset** | **Integer**| The initial index from which to return the results. | [optional] 

### Return type

[**InlineResponse2004**](InlineResponse2004.md)

### Authorization

[BearerAdmin](../README.md#BearerAdmin), [BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **profile_partial_update**
> ProfileDetail profile_partial_update(public_id, data)

Partial Update

Performs a partial update

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ProfileApi.new

public_id = 'public_id_example' # String | 

data = WeFitter::ProfileDetail.new # ProfileDetail | 


begin
  #Partial Update
  result = api_instance.profile_partial_update(public_id, data)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ProfileApi->profile_partial_update: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **public_id** | **String**|  | 
 **data** | [**ProfileDetail**](ProfileDetail.md)|  | 

### Return type

[**ProfileDetail**](ProfileDetail.md)

### Authorization

[BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **profile_read**
> ProfileDetail profile_read(public_id, )

Get Profile

Gets a single profile

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ProfileApi.new

public_id = 'public_id_example' # String | 


begin
  #Get Profile
  result = api_instance.profile_read(public_id, )
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ProfileApi->profile_read: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **public_id** | **String**|  | 

### Return type

[**ProfileDetail**](ProfileDetail.md)

### Authorization

[BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **profile_sleep_detail_list**
> Array&lt;SleepDetail&gt; profile_sleep_detail_list(profile_public_id, opts)

Sleep detail

Provides sleep detail data.

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ProfileApi.new

profile_public_id = 'profile_public_id_example' # String | 

opts = { 
  cursor: 'cursor_example', # String | The pagination cursor value.
  page_size: 56, # Integer | Number of results to return per page.
  date_start: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Lower bound of date or timestamp.
  date_end: DateTime.parse('2013-10-20T19:20:30+01:00') # DateTime | Upper bound of date or timestamp.
}

begin
  #Sleep detail
  result = api_instance.profile_sleep_detail_list(profile_public_id, opts)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ProfileApi->profile_sleep_detail_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profile_public_id** | **String**|  | 
 **cursor** | **String**| The pagination cursor value. | [optional] 
 **page_size** | **Integer**| Number of results to return per page. | [optional] 
 **date_start** | **DateTime**| Lower bound of date or timestamp. | [optional] 
 **date_end** | **DateTime**| Upper bound of date or timestamp. | [optional] 

### Return type

[**Array&lt;SleepDetail&gt;**](SleepDetail.md)

### Authorization

[BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **profile_sleep_summary_list**
> Array&lt;SleepSummary&gt; profile_sleep_summary_list(profile_public_id, opts)

Sleep Summary

Provides summarized sleep data.

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ProfileApi.new

profile_public_id = 'profile_public_id_example' # String | 

opts = { 
  cursor: 'cursor_example', # String | The pagination cursor value.
  page_size: 56, # Integer | Number of results to return per page.
  date_start: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Lower bound of date or timestamp.
  date_end: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Upper bound of date or timestamp.
  detail: true # BOOLEAN | If set to true and only 1 result is returned, it will add a detail field
}

begin
  #Sleep Summary
  result = api_instance.profile_sleep_summary_list(profile_public_id, opts)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ProfileApi->profile_sleep_summary_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profile_public_id** | **String**|  | 
 **cursor** | **String**| The pagination cursor value. | [optional] 
 **page_size** | **Integer**| Number of results to return per page. | [optional] 
 **date_start** | **DateTime**| Lower bound of date or timestamp. | [optional] 
 **date_end** | **DateTime**| Upper bound of date or timestamp. | [optional] 
 **detail** | **BOOLEAN**| If set to true and only 1 result is returned, it will add a detail field | [optional] 

### Return type

[**Array&lt;SleepSummary&gt;**](SleepSummary.md)

### Authorization

[BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **profile_stress_samples_list**
> Array&lt;StressSample&gt; profile_stress_samples_list(profile_public_id, opts)

Stress Sample

Presents stress samples.

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ProfileApi.new

profile_public_id = 'profile_public_id_example' # String | 

opts = { 
  cursor: 'cursor_example', # String | The pagination cursor value.
  page_size: 56, # Integer | Number of results to return per page.
  date_start: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Lower bound of date or timestamp.
  date_end: DateTime.parse('2013-10-20T19:20:30+01:00') # DateTime | Upper bound of date or timestamp.
}

begin
  #Stress Sample
  result = api_instance.profile_stress_samples_list(profile_public_id, opts)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ProfileApi->profile_stress_samples_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profile_public_id** | **String**|  | 
 **cursor** | **String**| The pagination cursor value. | [optional] 
 **page_size** | **Integer**| Number of results to return per page. | [optional] 
 **date_start** | **DateTime**| Lower bound of date or timestamp. | [optional] 
 **date_end** | **DateTime**| Upper bound of date or timestamp. | [optional] 

### Return type

[**Array&lt;StressSample&gt;**](StressSample.md)

### Authorization

[BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **profile_stress_summary_list**
> Array&lt;StressSummary&gt; profile_stress_summary_list(profile_public_id, opts)

Stress Summary

Presents stress summaries.

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ProfileApi.new

profile_public_id = 'profile_public_id_example' # String | 

opts = { 
  cursor: 'cursor_example', # String | The pagination cursor value.
  page_size: 56, # Integer | Number of results to return per page.
  date_start: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Lower bound of date or timestamp.
  date_end: DateTime.parse('2013-10-20T19:20:30+01:00') # DateTime | Upper bound of date or timestamp.
}

begin
  #Stress Summary
  result = api_instance.profile_stress_summary_list(profile_public_id, opts)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ProfileApi->profile_stress_summary_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profile_public_id** | **String**|  | 
 **cursor** | **String**| The pagination cursor value. | [optional] 
 **page_size** | **Integer**| Number of results to return per page. | [optional] 
 **date_start** | **DateTime**| Lower bound of date or timestamp. | [optional] 
 **date_end** | **DateTime**| Upper bound of date or timestamp. | [optional] 

### Return type

[**Array&lt;StressSummary&gt;**](StressSummary.md)

### Authorization

[BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **profile_track_sample_list**
> Array&lt;TrackSample&gt; profile_track_sample_list(profile_public_id, opts)

Track Sample

Presents GPX track samples.

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ProfileApi.new

profile_public_id = 'profile_public_id_example' # String | 

opts = { 
  cursor: 'cursor_example', # String | The pagination cursor value.
  page_size: 56, # Integer | Number of results to return per page.
  date_start: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Lower bound of date or timestamp.
  date_end: DateTime.parse('2013-10-20T19:20:30+01:00') # DateTime | Upper bound of date or timestamp.
}

begin
  #Track Sample
  result = api_instance.profile_track_sample_list(profile_public_id, opts)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ProfileApi->profile_track_sample_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profile_public_id** | **String**|  | 
 **cursor** | **String**| The pagination cursor value. | [optional] 
 **page_size** | **Integer**| Number of results to return per page. | [optional] 
 **date_start** | **DateTime**| Lower bound of date or timestamp. | [optional] 
 **date_end** | **DateTime**| Upper bound of date or timestamp. | [optional] 

### Return type

[**Array&lt;TrackSample&gt;**](TrackSample.md)

### Authorization

[BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **profile_update**
> ProfileDetail profile_update(public_id, data)

Update profile

Updates the specified profile, excluding ID, team and public id.

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ProfileApi.new

public_id = 'public_id_example' # String | 

data = WeFitter::ProfileDetail.new # ProfileDetail | 


begin
  #Update profile
  result = api_instance.profile_update(public_id, data)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ProfileApi->profile_update: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **public_id** | **String**|  | 
 **data** | [**ProfileDetail**](ProfileDetail.md)|  | 

### Return type

[**ProfileDetail**](ProfileDetail.md)

### Authorization

[BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **profile_weight_list**
> Array&lt;Weight&gt; profile_weight_list(profile_public_id, opts)

Weight

Lists changes in weight for the profile.

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ProfileApi.new

profile_public_id = 'profile_public_id_example' # String | 

opts = { 
  cursor: 'cursor_example', # String | The pagination cursor value.
  page_size: 56, # Integer | Number of results to return per page.
  date_start: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Lower bound of date or timestamp.
  date_end: DateTime.parse('2013-10-20T19:20:30+01:00') # DateTime | Upper bound of date or timestamp.
}

begin
  #Weight
  result = api_instance.profile_weight_list(profile_public_id, opts)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ProfileApi->profile_weight_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profile_public_id** | **String**|  | 
 **cursor** | **String**| The pagination cursor value. | [optional] 
 **page_size** | **Integer**| Number of results to return per page. | [optional] 
 **date_start** | **DateTime**| Lower bound of date or timestamp. | [optional] 
 **date_end** | **DateTime**| Upper bound of date or timestamp. | [optional] 

### Return type

[**Array&lt;Weight&gt;**](Weight.md)

### Authorization

[BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **profile_workout_list**
> Array&lt;Workout&gt; profile_workout_list(profile_public_id, opts)

Workout

Lists workouts associated with the profile. Used to be known as Activity.

### Example
```ruby
# load the gem
require 'wefitter-gem'
# setup authorization
WeFitter.configure do |config|
  # Configure API key authorization: BearerProfile
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = WeFitter::ProfileApi.new

profile_public_id = 'profile_public_id_example' # String | 

opts = { 
  cursor: 'cursor_example', # String | The pagination cursor value.
  page_size: 56, # Integer | Number of results to return per page.
  date_start: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Lower bound of date or timestamp.
  date_end: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Upper bound of date or timestamp.
  deduplicate: true, # BOOLEAN | If set to true, duplicates will be aggregated.
  heart_rate_detail: true # BOOLEAN | If set to true and page_size is 1, it will add heart_rate_samples field
}

begin
  #Workout
  result = api_instance.profile_workout_list(profile_public_id, opts)
  p result
rescue WeFitter::ApiError => e
  puts "Exception when calling ProfileApi->profile_workout_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profile_public_id** | **String**|  | 
 **cursor** | **String**| The pagination cursor value. | [optional] 
 **page_size** | **Integer**| Number of results to return per page. | [optional] 
 **date_start** | **DateTime**| Lower bound of date or timestamp. | [optional] 
 **date_end** | **DateTime**| Upper bound of date or timestamp. | [optional] 
 **deduplicate** | **BOOLEAN**| If set to true, duplicates will be aggregated. | [optional] 
 **heart_rate_detail** | **BOOLEAN**| If set to true and page_size is 1, it will add heart_rate_samples field | [optional] 

### Return type

[**Array&lt;Workout&gt;**](Workout.md)

### Authorization

[BearerProfile](../README.md#BearerProfile)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



