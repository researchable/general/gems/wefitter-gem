# WeFitter::ProfileDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **String** |  | [optional] 
**public_id** | **String** |  | [optional] 
**given_name** | **String** |  | [optional] 
**family_name** | **String** |  | [optional] 
**nickname** | **String** |  | [optional] 
**gender** | **String** |  | [optional] 
**birthdate** | **Date** |  | [optional] 
**zoneinfo** | **String** |  | [optional] 
**locale** | **String** |  | [optional] 
**total_calories** | **Float** |  | [optional] 
**total_distance** | **Float** |  | [optional] 
**total_steps** | **Float** |  | [optional] 
**total_points** | **Float** |  | [optional] 
**reference** | **String** |  | [optional] 
**teams** | [**Array&lt;TeamProfile&gt;**](TeamProfile.md) | Team of which this profile is a member of | [optional] 
**avatar** | **String** |  | [optional] 
**created_at** | **DateTime** |  | [optional] 
**updated_at** | **DateTime** |  | [optional] 
**num_active_challenges** | **String** |  | [optional] 
**num_done_challenges** | **String** |  | [optional] 
**num_future_challenges** | **String** |  | [optional] 


