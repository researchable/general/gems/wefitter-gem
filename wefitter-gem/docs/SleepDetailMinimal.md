# WeFitter::SleepDetailMinimal

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | **DateTime** | Starting time of the measurement. | 
**timestamp_end** | **DateTime** | Ending time of the measurement. | [optional] 
**level** | **String** | Sleep level | 


