# WeFitter::RecommendedProducts

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**article_number** | **Integer** |  | [optional] 
**name** | **String** |  | 
**price** | **Float** |  | [optional] 
**description** | **String** |  | [optional] 


