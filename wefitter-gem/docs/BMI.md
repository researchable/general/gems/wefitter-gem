# WeFitter::BMI

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | **DateTime** | Starting time of the measurement. | 
**source** | **String** | Origin of the data. | 
**is_manual** | **BOOLEAN** | Whether the data was entered manually. | [optional] 
**bmi** | **Float** | Body Mass Index, automatically calculated if not entered and both weight and height are available. | [optional] 


