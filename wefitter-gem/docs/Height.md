# WeFitter::Height

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | **DateTime** | Starting time of the measurement. | 
**source** | **String** | Origin of the data. | 
**is_manual** | **BOOLEAN** | Whether the data was entered manually. | [optional] 
**height** | **Float** | Height in cm. | [optional] 


