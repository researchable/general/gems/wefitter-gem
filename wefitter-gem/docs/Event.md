# WeFitter::Event

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | **DateTime** | Time at which the action was performed. | [optional] 
**points** | **Float** | Amount of points awarded for the action. | [optional] 
**description** | **String** | Title / description of the action. | [optional] 


