# WeFitter::ChallengeList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **String** |  | [optional] 
**public_id** | **String** |  | [optional] 
**title** | **String** |  | 
**slogan** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**start** | **DateTime** |  | 
**_end** | **DateTime** |  | [optional] 
**type** | **String** |  | 
**goal** | **String** |  | 
**goal_value** | **Integer** |  | [optional] 
**goal_type** | **String** |  | 
**visibility** | **String** |  | 
**num_members** | **String** |  | [optional] 
**repetition** | **String** |  | 
**avatar** | **String** |  | [optional] 
**data_source** | **String** |  | [optional] 
**activity_types** | [**Array&lt;ChallengeActivityType&gt;**](ChallengeActivityType.md) |  | [optional] 
**current_period_start** | **DateTime** | The start (exclusive) of the current period. Null if the challenge has not started yet. | [optional] 
**current_period_end** | **DateTime** | The end (exclusive) of the current period. Null if the challenge end has passed. | [optional] 
**goal_cap** | **BOOLEAN** | Whether scores should be unable to surpass the goal value. | [optional] 


