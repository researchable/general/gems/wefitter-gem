=begin
#WeFitter API

#This is the WeFitter API

OpenAPI spec version: v1.3
Contact: hello@wefitter.com
Generated by: https://github.com/swagger-api/swagger-codegen.git
Swagger Codegen version: 2.4.38
=end

require 'spec_helper'
require 'json'
require 'date'

# Unit tests for WeFitter::ProfilePublicIdsListAndTeamAsPublicId
# Automatically generated by swagger-codegen (github.com/swagger-api/swagger-codegen)
# Please update as you see appropriate
describe 'ProfilePublicIdsListAndTeamAsPublicId' do
  before do
    # run before each test
    @instance = WeFitter::ProfilePublicIdsListAndTeamAsPublicId.new
  end

  after do
    # run after each test
  end

  describe 'test an instance of ProfilePublicIdsListAndTeamAsPublicId' do
    it 'should create an instance of ProfilePublicIdsListAndTeamAsPublicId' do
      expect(@instance).to be_instance_of(WeFitter::ProfilePublicIdsListAndTeamAsPublicId)
    end
  end
  describe 'test attribute "profiles"' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  describe 'test attribute "is_active"' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  describe 'test attribute "team"' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  describe 'test attribute "joined"' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

end
