#!/usr/bin/env ruby

# Gem loaded from path
require 'bundler/inline'

puts '[INFO] Starting the WeFitter POC script'
gemfile do
  source 'https://rubygems.org'
  gem 'wefitter-gem', path: './wefitter-gem'
end

require 'wefitter-gem'
require "base64"
require "securerandom"

puts '[INFO] Setting up the configuration'
base_config = WeFitter::Configuration.new do |config|
  config.scheme = 'https'
  config.host = 'api.wefitter.com'
  config.base_path = '/api/v1.3'

  # Set the API key
  # Create credentials here: https://dashboard.wefitter.com/appCredentials
  config.username = ENV['API_KEY']
  config.password = ENV['API_SECRET']
  config.debugging = false

  # Note that in the first stage of the API, the API key is sent as a Basic
  # Auth, so we can retrieve the bearer auth
  config.api_key_prefix['Authorization'] = 'Basic'
end

# First create the token. This will return the bearer token. 
# I'm not sure what the params for token_create are.
puts '[INFO] Creating token'
client = WeFitter::ApiClient.new(base_config)
token = WeFitter::TokenApi.new(client).token_create('test-token')

# Now we can use the bearer token to create a team / do other interactions with
# the api
base_config.api_key_prefix['Authorization'] = 'Bearer'
base_config.api_key['Authorization'] = token.bearer

client = WeFitter::ApiClient.new(base_config)
teamApi = WeFitter::TeamApi.new(client)

puts '[INFO] Deleting old teams'
teams = teamApi.team_list
teams.results.each do |team_tmp|
  puts "[INFO] Deleting team #{team_tmp.public_id}"
  teamApi.team_delete(team_tmp.public_id)
end

puts '[INFO] Creating test team'
team = teamApi.team_create(
  { 
    name: 'Team name', 
    description: 'This is a test team created from the api' 
  }
)

# Delete all profiles to clean up earlier runs
profileApi = WeFitter::ProfileApi.new(client)
profiles = profileApi.profile_list 

puts '[INFO] Deleting all profiles'
profiles.results.each do |profile_tmp|
  puts "[INFO] Deleting profile #{profile_tmp.public_id}"
  profileApi.profile_delete(profile_tmp.public_id)
end

# Create a new one
puts '[INFO] Creating new profile'
profile = profileApi.profile_create({name: 'Researchable test user', reference: SecureRandom.uuid})

# Sign in as the user, and hookup strava
base_config.api_key_prefix['Authorization'] = 'Bearer'
base_config.api_key['Authorization'] = profile.bearer
connectionApi = WeFitter::ConnectionApi.new(client)

# Strava
puts '[INFO] Creating new Strava connection'
strava_result = connectionApi.connection_connect_create('STRAVA', {profile: profile.public_id})
puts '[INFO] Click on the following link to connect your Strava account'
puts "[INFO] #{strava_result._next}"

# Fitbit
puts '[INFO] Creating new Fitbit connection'
fitbit_result = connectionApi.connection_connect_create('FITBIT', {profile: profile.public_id})
puts '[INFO] Click on the following link to connect your Fitbit account'
puts "[INFO] #{fitbit_result._next}"

# Garmin
puts '[INFO] Creating new Garmin connection'
garmin_result = connectionApi.connection_connect_create('GARMIN', {profile: profile.public_id})
puts '[INFO] Click on the following link to connect your Garmin account'
puts "[INFO] #{garmin_result._next}"

# Polar
puts '[INFO] Creating new Polar connection'
polar_result = connectionApi.connection_connect_create('POLAR', {profile: profile.public_id})
puts '[INFO] Click on the following link to connect your Polar account'
puts "[INFO] #{polar_result._next}"

puts "[INFO] Done"
