FROM ruby:3.1.2

# Default directory
WORKDIR /app

COPY . .

# Install rails
#RUN chown -R user:user /opt/app

# Run a shell
CMD ["/app/test.rb"]
